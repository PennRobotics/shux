import asyncio

import protocol
from api import MVX2UApi

if __name__ == '__main__':
    async def main():
        api = MVX2UApi()

        #identity = await api.request_identity()
        #if identity != protocol.MVX2U_FINGERPRINT:
        #    raise Exception('Failure to identify, {0}'.format(identity))

        async def req():
            return {
                    "48": await api.request_48v_enabled(),
                    }

        print(await req())

    asyncio.run(main())
