from collections import namedtuple

PEQBand = namedtuple("PEQBand", "id frequencyHz gainDbEnum gainDbx10 enabledEnum enabled")  # noqa
