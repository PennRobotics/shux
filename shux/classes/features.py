from enum import Enum

class Feat(Enum):
    LOCK     = (b'\x00\xA6', 1, 'lock',          '_configLock')
    M_GAIN   = (b'\x01\x02', 2, 'manual_gain',   '_inputGain')  # Each 0.5 dB = 32h (16-bit)
    MUTE     = (b'\x01\x04', 1, 'mute',          '_micMute')
    HPF      = (b'\x01\x06', 1, 'hpf',           '_highPassFilterMode')
    LIMIT    = (b'\x01\x51', 1, 'limiter',       '_limiterSelect')
    COMP     = (b'\x01\x5C', 1, 'compressor',    '_compressorMode')  # double check this!! (should be 0 to 3, right? only 0 to 2 in data dump; TODO)
    VOLTS    = (b'\x01\x66', 1, 'phantom',       '_phantomPowerVolts')  # 0 or 48 (0x30)
    A_POS    = (b'\x01\x82', 1, 'auto_position', '_autoLevelPosition')  # 1=far
    A_TONE   = (b'\x01\x83', 1, 'auto_tone',     '_autoLevelTone')  # 0=dark, 1=nat, 2=bright
    AUTO     = (b'\x01\x85', 1, 'auto',          '_autoLevel')  # 1=auto
    MIX      = (b'\x01\x86', 1, 'mix',           '_monitorMix')  # 64h (100) is full mic
    A_GAIN   = (b'\x01\x87', 4, 'auto_gain',     '_autoLevelConfig')  # 0, 1, 2  - TODO: sure about this name?
    EQ       = (b'\x02\x00', 1, 'eq',            '_peqEnabled')
    EQ100_EN = (b'\x02\x10', 1, 'band1_state',   '_peqBands[0].enabled')
    EQ100    = (b'\x02\x14', 2, 'band1_gain',    '_peqBands[0].gainDbx10')
    EQ250_EN = (b'\x02\x20', 1, 'band2_state',   '_peqBands[1].enabled')
    EQ250    = (b'\x02\x24', 2, 'band2_gain',    '_peqBands[1].gainDbx10')
    EQ1K_EN  = (b'\x02\x30', 1, 'band3_state',   '_peqBands[2].enabled')
    EQ1K     = (b'\x02\x34', 2, 'band3_gain',    '_peqBands[2].gainDbx10')
    EQ4K_EN  = (b'\x02\x40', 1, 'band4_state',   '_peqBands[3].enabled')
    EQ4K     = (b'\x02\x44', 2, 'band4_gain',    '_peqBands[3].gainDbx10')
    EQ10K_EN = (b'\x02\x50', 1, 'band5_state',   '_peqBands[4].enabled')
    EQ10K    = (b'\x02\x54', 2, 'band5_gain',    '_peqBands[4].gainDbx10')

    def __new__(cls, value, bytelen, args_var, cls_var):
        assert type(value) is bytes
        assert type(bytelen) is int
        assert type(args_var) is str
        assert cls_var[0] == '_'

        obj = object.__new__(cls)
        obj._value_ = value
        obj.bytelen = bytelen
        obj.args_var = args_var
        obj.cls_var = cls_var
        return obj
