from classes.features import *
from utils import *

DEBUG = print  # TODO

class DummyMVX2U:
    num_sent = 0
    num_resp = 0
    loopback_queue = []
    # TODO: revise all these once switch to HID is finished

    def __init__(self):
        DEBUG('An object of DummyMVX2U class was initialized')
        pass

    def __repr__(self):
        return 'DummyMVX2U()'

    def __iter__(self):
        yield []

    def reset(self):
        return None

    def set_nonblocking(self, *args):
        pass

    def get_manufacturer_string(self):
        return "Dummy (not Shure)"

    def get_product_string(self):
        return "Dummy MVX2U"

    def get_active_configuration(self):
        return None

    def set_configuration(self):
        pass

    def write(self, msg):
        #DEBUG('[DUMMY] write', HexEm(msg))
        self.loopback_queue.append(msg)
        self.num_sent += 1

    def read(self, *arg, **kwargs):
        if self.num_sent <= self.num_resp:
            return 0
        spoofed_msg = [n for n in self.loopback_queue.pop(0)]  # Convert from bytes to array
        spoofed_msg[10] += 2  # Change from CMD_ to RES_; TODO: deal with response
        response_key = Feat(bytes(spoofed_msg[14:16]))
        # Adjust length to include response
        response_len = response_key.bytelen
        spoofed_msg[1] += response_len
        spoofed_msg[7] += response_len
        spoofed_msg[9] += response_len
        spoofed_msg = spoofed_msg[:0x10] + [0] * response_len
        spoofed_msg = CrcEm(bytes(spoofed_msg))
        spoofed_msg = PadEm(spoofed_msg)
        #DEBUG('[DUMMY] read', HexEm(spoofed_msg))
        return spoofed_msg

    def read_message(self):
        yield None

    def interpret_packet(self, *args):
        self.num_resp += 1
