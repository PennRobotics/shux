import argparse

from _version import __version__

class MVParser():
    _VOLT_CHOICES = {'off': 0, 'on': 48}
    _MODE_CHOICES = {'manual': 0, 'auto': 1}
    _AUTO_GAIN_CHOICES = {'quiet': 0, 'normal': 1, 'loud': 2}
    _AUTO_POSITION_CHOICES = {'near': 0, 'far': 1}
    _AUTO_TONE_CHOICES = {'dark': 0, 'natural': 1, 'bright': 2}
    _EQ_CHOICES = {'100': 0, '250': 1, '1k': 2, '4k': 3, '10k': 4}
    _COMPRESSOR_CHOICES = {'off': 0, 'light': 1, 'medium': 2, 'heavy': 3}
    _ON_OFF_CHOICES = {'off': 0, 'on': 1}
    _HPF_CHOICES = {'off': 0, '75hz': 75, '150hz': 150}

    def __init__(self, *args, **kwargs):
        self.parser = argparse.ArgumentParser(
            description='Configures a Shure MVX2U microphone adapter',
            epilog='If no flags are used, all adapter settings are output',
            formatter_class=lambda prog: argparse.HelpFormatter(prog, max_help_position=50),
            add_help=False)
        self.parser.add_argument('-?', '--help', action="help", default=argparse.SUPPRESS, help='show this help message and exit')  # noqa
        self.parser.add_argument('-v', '--version', action="version", version='%(prog)s {version}'.format(version=__version__))  # noqa
        self.parser.add_argument('-q', '--quiet', action="count", help='change configuration with minimal output')  # noqa  # TODO: improve filtering of -q and -qq
        self.parser.add_argument('--debug', action='store_true', help='print debug messages')  # noqa
        self.parser.add_argument('--dummy', action='store_true', help=argparse.SUPPRESS)
        self.parser.add_argument('--apply-unchanged', action='store_true', help=argparse.SUPPRESS)

        self.parser.add_argument('-L', '--lock', action='store_true', default=None, help='lock settings after changes')  # noqa
        self.parser.add_argument('-U', '--unlock', action='store_true', default=None, help='unlock settings before changes')  # noqa

        mute_group = self.parser.add_mutually_exclusive_group()
        mute_group.add_argument('--mute', '-m', dest='mute', action='store_true', default=None, help='mute mic')  # noqa
        mute_group.add_argument('--unmute', dest='mute', action='store_false', default=None, help='unmute mic')  # noqa

        # self.parser.add_argument('--preset')  # TODO

        self.add_dict_argument(self._VOLT_CHOICES, '--phantom', '-p')

        mix_arg = self.parser.add_argument('--mix', '--usb-in', type=int, help=argparse.SUPPRESS)  # noqa
        self.parser.add_argument('--playback-mix', type=int, help='%% of monitor mix from the connected device', metavar='<int>')  # noqa
        self.parser.add_argument('--mic-mix', type=int, help='%% of monitor mix from the mic', metavar='<int>')  # noqa
        only_group = self.parser.add_mutually_exclusive_group()
        only_group.add_argument('--only-playback', '--only-usb', action='store_true', default=None, help='set monitor mix: 0%% mic, 100%% playback')  # noqa
        only_group.add_argument('--only-mic', action='store_true', default=None, help='set monitor mix: 100%% mic, 0%% playback')  # noqa

        mode_group = self.parser.add_mutually_exclusive_group()
        mode_group.add_argument('--mode', dest='auto', nargs=1, choices=self._MODE_CHOICES, help='set auto or manual mode', metavar='<manual|auto>,  --manual,  --auto ')  # noqa
        mode_group.add_argument('--manual', dest='auto', action='store_false', default=None, help=argparse.SUPPRESS)  # noqa
        mode_group.add_argument('--auto', dest='auto', action='store_true', default=None, help=argparse.SUPPRESS)  # noqa

        self.add_dict_argument(self._AUTO_GAIN_CHOICES, '--auto-gain')
        self.add_dict_argument(self._AUTO_POSITION_CHOICES, '--auto-position')
        self.add_dict_argument(self._AUTO_TONE_CHOICES, '--auto-tone')



        # TODO/FIXME: --manual-gain causing errors
        #self.parser.add_argument('--gain', '--manual-gain', '-g', dest='manual_gain', type=self.manual_gain_type, help='microphone gain in dB (manual)', metavar='<num>')  # noqa
        self.parser.add_argument('--gain', '--manual-gain', '-g', dest='manual_gain', type=int, help='microphone gain in dB (manual)', metavar='<num>')  # noqa

        self.parser.add_argument('--reset-eq', action='store_true', default=None, help='reset EQ bands before applying any -b/--eqband settings (manual)')  # noqa
        self.add_dict_argument(self._EQ_CHOICES, '--band-state', '-b', eq_case=True)
        self.parser.add_argument('--band1-state', action='store', nargs=1, choices=[0, 1], help=argparse.SUPPRESS)
        self.parser.add_argument('--band2-state', action='store', nargs=1, choices=[0, 1], help=argparse.SUPPRESS)
        self.parser.add_argument('--band3-state', action='store', nargs=1, choices=[0, 1], help=argparse.SUPPRESS)
        self.parser.add_argument('--band4-state', action='store', nargs=1, choices=[0, 1], help=argparse.SUPPRESS)
        self.parser.add_argument('--band5-state', action='store', nargs=1, choices=[0, 1], help=argparse.SUPPRESS)
        self.add_dict_argument(self._EQ_CHOICES, '--band-gain', '--db', eq_case=True)
        self.parser.add_argument('--band1-gain', action='store', nargs=1, choices=[-8, -6, -4, -2, 0, 2, 4, 6], help=argparse.SUPPRESS)
        self.parser.add_argument('--band2-gain', action='store', nargs=1, choices=[-8, -6, -4, -2, 0, 2, 4, 6], help=argparse.SUPPRESS)
        self.parser.add_argument('--band3-gain', action='store', nargs=1, choices=[-8, -6, -4, -2, 0, 2, 4, 6], help=argparse.SUPPRESS)
        self.parser.add_argument('--band4-gain', action='store', nargs=1, choices=[-8, -6, -4, -2, 0, 2, 4, 6], help=argparse.SUPPRESS)
        self.parser.add_argument('--band5-gain', action='store', nargs=1, choices=[-8, -6, -4, -2, 0, 2, 4, 6], help=argparse.SUPPRESS)

        self.add_dict_argument(self._ON_OFF_CHOICES, '--eq', '-e')
        self.add_dict_argument(self._COMPRESSOR_CHOICES, '--compressor', '-c')
        self.add_dict_argument(self._HPF_CHOICES, '--hpf', '-h', h_case=True)
        self.add_dict_argument(self._ON_OFF_CHOICES, '--limiter', '-l')

    def parse_args(self):
        return self.parser.parse_args()

    def manual_gain_type(gain_str):
        gain = float(gain_str)
        if 2 * gain != int(2 * gain) or gain < 0 or gain > 60:
            msg = 'Gain should range from 0 to 60 in increments of 0.5'
            raise argparse.ArgumentTypeError(msg)
        return gain

    def add_dict_argument(self, choice_dict, *arg_list, eq_case=False, h_case=False):
        kv_2d = [(v, k) for k, v in choice_dict.items()]
        kv_flat = [str(x) for y in kv_2d for x in y]
        s = ' | '.join(str(el) for el in kv_flat)

        if eq_case:
            db_vals = 'gain' in arg_list[0]
            hs = f'val1 = {s}, val2 = {"-6 .. 6" if db_vals else "0 | 1"}'
            self.parser.add_argument(*arg_list, action='append', nargs=2, choices=kv_flat, help=hs, metavar=f'<val>')
        elif h_case:
            self.parser.add_argument(*arg_list, action='store', nargs='?', const=-1, default=None, choices=kv_flat, help=f'val = {s}', metavar=f'<val>')
        else:
            self.parser.add_argument(*arg_list, action='store', choices=kv_flat, help=f'val = {s}', metavar=f'<val>')

