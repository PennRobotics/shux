import threading
import time

DEBUG = print  # TODO

class MVX2USubscriber(threading.Thread):
    stopped = False
    mvx2u_object = None

    def __init__(self, mvx2u_object):
        threading.Thread.__init__(self, daemon=True)
        self.mvx2u_object = mvx2u_object
        DEBUG('An object of MVX2USubscriber/Thread class was initialized')

    def __del__(self):
        DEBUG('Delete')

    def run(self):
        while not self.mvx2u_object.subscriber_should_close:
            self.stopped = self.mvx2u_object.num_sent <= self.mvx2u_object.num_resp
            while not self.stopped and not self.mvx2u_object.subscriber_should_close:
                # TODO: what is the deal with get_input_report()?
                received_packet = self.mvx2u_object.mv_dev.read(64, timeout_ms=50)
                if received_packet:
                    self.mvx2u_object.interpret_packet(bytes(received_packet))
                time.sleep(0.01)
                self.stopped = self.mvx2u_object.num_sent <= self.mvx2u_object.num_resp
            time.sleep(0.1)
