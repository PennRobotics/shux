#!/usr/bin/env python3

#   Shux: Shure with Linux
#
#   a utility for programming the Shure MVX2U microphone adapter
#
#   Copyright 2024 Brian Wright
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

import atexit
import os
import platform
import sys

from classes.mvparser import *
from classes.mvx2u import *
from customtype import PEQBand

WINDOWS = True if platform.system() == 'Windows' else False

# Argument Handling
mvparser = MVParser()

args = mvparser.parse_args()
if args.quiet is None and args.debug is True:
    print(f'Arguments after parse_args(): {args}')


def arg_value_from_dict(arg, d):
    if arg is not None:
        if type(arg) is list and len(arg) == 1:
            arg = arg[0]
        if arg in d:
            return d[arg]
        else:
            return int(arg)


DEBUG = print
### def DEBUG(*a, **kw):
###     print(*a, **kw) if args.debug else print('', end='')


def restore_stdout_stderr():
    sys.stdout = os.fdopen(1, 'w')
    sys.stderr = os.fdopen(2, 'w')

_istdout = sys.stdout
if args.quiet is not None:
    sys.stdout = open(os.devnull, 'a')
    if args.quiet > 1:
        _istdout = sys.stdout
        sys.stderr = open(os.devnull, 'a')
atexit.register(restore_stdout_stderr)

if args.mute is not None:
    args.mute = 1 if args.mute is True else 0

if args.phantom is not None:
    # TODO/FIXME: _VOLT_CHOICES is not defined
    # TODO/FIXME: _COMPRESSOR_CHOICES is not defined
    # TODO/FIXME: (all the other _CHOICES? not defined)
    # TODO/FIXME: --phantom 1 and --phantom '1' should be aliases to 'on'
    args.phantom = arg_value_from_dict(args.phantom, mvparser._VOLT_CHOICES)

if args.auto is not None:
    args.auto = arg_value_from_dict(args.auto, mvparser._MODE_CHOICES)

if args.auto_gain is not None:
    args.auto_gain = arg_value_from_dict(args.auto_gain, mvparser._AUTO_GAIN_CHOICES)

if args.auto_position is not None:
    args.auto_position = arg_value_from_dict(args.auto_position, mvparser._AUTO_POSITION_CHOICES)

if args.auto_tone is not None:
    args.auto_tone = arg_value_from_dict(args.auto_tone, mvparser._AUTO_TONE_CHOICES)

# TODO: create dict to hold each band_state and band_gain. args should be max len 5 and dict is same length, otherwise throw error
#if args.band_state is not None:
#    args.band_state = arg_value_from_dict(args.band_state, _)
#
#if args.band_gain is not None:
#    args.band_gain = arg_value_from_dict(args.band_gain, _)

if args.compressor is not None:
    args.compressor = arg_value_from_dict(args.compressor, mvparser._COMPRESSOR_CHOICES)

if args.eq is not None:
    args.eq = arg_value_from_dict(args.eq, mvparser._ON_OFF_CHOICES)

if args.limiter is not None:
    args.limiter = arg_value_from_dict(args.limiter, mvparser._ON_OFF_CHOICES)

# Special cases
if args.hpf is not None:
    if args.hpf == -1:
        parser.print_help()
        print('\nNOTE: To use -h for HPF, a value must be given')
        sys.exit(0)
    args.hpf = arg_value_from_dict(args.hpf, mvparser._HPF_CHOICES)

# The way monitor mix is shown in the desktop application is a bit vague.
#   A mix of 50% is actually 100% microphone and 100% device playback
#   A mix of 90% will be 20% microphone and 100% device playback.
#   At least one device will be at 100% always:
#   the sum of the values should always be between 100 and 200.
#   However, most people would want a ratio, like 50/50, or 90/10, or 100/0.
#   --mix/--usb-in <val> will use the value from 0 to 100,
#   where 100 is "device playback only" and 50 is "both sources mixed at 100%"
#   Flags --playback-mix and --mic-mix should handle non-ratio cases:
#       -a 100  -b   0
#       -a 100  -b 100
#       -a 100  -b  50
#       -a  50  -b  50
#       -a  70  -b  30
#       -a 100  -b  60
#   Cases that should be invalid would include  -a 30 -b 40  and  -a 300 -b 50
#   One last detail: if one flag from the pair is given but not both,
#   we would assume the missing flag value is 100:
#   There are also flags for --only-playback, --only-usb, and --only-mic
#   which automatically set --mix to 0 or 100.
if args.only_playback is not None or args.only_mic is not None:
    if args.playback_mix is not None or args.mic_mix is not None or args.mix is not None:  # noqa
        raise argparse.ArgumentError(mix_arg, '--only-playback, --only-usb, and --only-mic should not be used with other monitor flags')  # noqa
    args.mix = 100 if args.only_playback else 0
if args.playback_mix is not None or args.mic_mix is not None:
    if args.mix is not None:
        raise argparse.ArgumentError(mix_arg, 'should not be used with --playback-mix or --mic-mix')  # noqa
    args.playback_mix = args.playback_mix if args.playback_mix is not None else 100 - args.mic_mix  # noqa
    args.mic_mix = args.mic_mix if args.mic_mix is not None else 100 - args.playback_mix  # noqa
    total = args.playback_mix + args.mic_mix  # TODO: see below
    if total < 100 or total > 200:
        raise argparse.ArgumentTypeError('--playback-mix and --mic-mix should range from 0 to 100 and add up to at least 100')  # noqa
    if total > 100 and (args.playback_mix != 100 and args.mic_mix != 100):
        raise argparse.ArgumentTypeError('At least one of the monitor flags should be 100. Alternatively, both can sum to 100.')  # noqa
    args.mix = (100 * args.playback_mix) // total  # TODO: this might be wrong, double-check math. For instance, a=100 b=50 should land at mix=75 (or 25)
    # TODO: When nums sum to 100, raise each value proportionately until one equals 100, then use the regular 100 to 200 formula
del args.playback_mix, args.mic_mix, args.only_playback, args.only_mic


# Execution
if __name__ == '__main__':
    mv = ShureMVX2U(args=args)
    mv.read_config()
    mv.find_changes()
    mv.print_config()
    mv.write_config()
    DEBUG('Waiting for last-minute incoming messages')
    time.sleep(1.2)
    sys.exit(0)
