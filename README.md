Shux
====
_&hellip;friends, your dark days are at an end!_

**Shux** is **Shure (MOTIV) using Linux**: a configuration client for the MVX2U microphone adapter.

For Android, see [ShureDroid](https://github.com/PennRobotics/ShureDroid)


## MVX2U Users

### Current Efforts

_Protocol rewrite with simpler syntax and hopefully improved async_

_A Tkinter GUI is being developed for interactively changing settings
very similar to the Motiv Desktop application._

**PLEASE USE TAGS TO FIND A VERSION WITH LEAST ERRORS!** In short, I
have not had the time, focus, and motivation to get everything working
on the MVX2U how I would like. There are a lot of partially explored
changes that are not git-friendly (edits pushed directly to _main_,
unfixed issues, mismatched documentation, pycodestyle warnings) and
it is always unclear when I will get back to this project.

#### Checklist

- [ ] .gitignore (including .venv)
- [ ] split code into branches
    - [x] bigassmess (temporary, everything as it is now)
    - [x] main
    - [x] develop
    - [ ] dummy
    - [ ] cli
    - [ ] gui
    - [ ] simulaudio (allow access to audio endpoints while adjusting configuration)
    - [x] babel/packeteer (used for stress testing MVX2U protocol)
    - [ ] fwupd (updating shure firmware)
    - [ ] remove rework branch
- [ ] clear out unneeded existing issues
- [ ] delete bigassmess branch and put any unused but useful work into a snippet or wiki
    - [ ] the "re" stuff can go into the other repo
        - [ ] find the original unmerged "re" folder (check external drive)
- [ ] remove setup in favor of explicit venv/pip instructions
    - [x] in-progress on the _develop_ branch
- [ ] create a working test suite
- [ ] once a fully functional connection is made? do Python packaging

### Start

_Plug in the adapter after loading the udev rules, or right before running the
Python script. There is a check which uses **lsusb** although this can be
skipped/removed after the device works. You will also need **git** (or else
download the **Shux** source manually) and **udev** and **pip** and **python**._

```sh
git clone https://gitlab.com/PennRobotics/shux.git
cd shux
sh ./setup.sh
source .env/bin/activate
sudo udevadm control --reload-rules && sudo udevadm trigger
[[ $(lsusb -d 14ed:1013) ]] && echo "Ready" || echo "Connect MVX2U to a USB port"
cd shux
python ./shux.py
```

If there are problems installing the Python HID module, refer to the
[hidapi documentation](https://github.com/trezor/cython-hidapi).

Help this project succeed!
**Please report the first place where friction is found while using this project.**


### Examples

View current configuration:

```sh
python ./shux.py
```

-----

Turn off phantom power:

```sh
./shux.py --unlock --phantom off
```

-----

See available flags:

```sh
./shux.py -h
```

-----

Set high-pass filter (HPF) to 75 Hz:

```sh
./shux.py -h 75hz
```

-----

Unlock device, showing raw messages:

```sh
./shux.py --unlock --debug
```

-----

A more advanced test case:

```sh
./shux.py --dummy --unlock --phantom on --limiter 0 --lock --apply-unchanged
```


## Need more info?

Read the [full documentation](docs/index.md)


## Prefer Android?

A work-in-progress APK is available at https://github.com/PennRobotics/ShureDroid


## Prefer a GUI?

A refactor of the device access code and Tkinter GUI are in the **gui** subfolder


## Prefer a browser-based solution?

A WebHID client is in the works: [Shhid](https://gitlab.com/PennRobotics/shhid)


## License

**Shux** is made available under the [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0) license. Original source is at https://gitlab.com/PennRobotics/shux/ and the following guidelines shall apply:

1. The original copyright notice must be included without changes to content whether the affiliated code is modified or not.
2. Include the full Apache 2.0 license text with the code.
3. State significant changes made to the code.

Those painless but important and necessary conditions afford users of this code the vast freedom to:

* change the original or any derived code
* distribute the original or any derivative, even commercially, as source and/or a binary without the need to disclose the source, even under other compatible licenses

```text
   Copyright 2024 Brian Wright

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
```
