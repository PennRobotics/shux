set -x

# for real device testing, call script with this set to null
: ${WHEN_ACTIVE_DUMMY_FLAG=--dummy}

# optionally, put this between each script
#read -p "Proceed?"
#../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --debug --dummy || exit $?

### LOCK ###
### PHANTOM ###
sleep 0.3
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --unlock || exit $?
sleep 2
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --phantom on || exit $?
sleep 2
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --phantom off || exit $?
sleep 2
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --lock || exit $?
sleep 2
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --phantom on || exit $?
sleep 2
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --phantom off || exit $?
sleep 2
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --apply-unchanged --phantom off || exit $?
sleep 2
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --lock || exit $?
sleep 2
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --unlock || exit $?
sleep 2
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --unlock || exit $?
sleep 2
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --unlock --phantom on --lock || exit $?
sleep 2
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --unlock --phantom off --lock || exit $?
sleep 2
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --unlock --phantom on --lock || exit $?
sleep 2
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --unlock --phantom off --lock || exit $?
sleep 2
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --apply-unchanged --unlock --phantom off --lock || exit $?
sleep 2
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --unlock || exit $?
# aliases
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG -L || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG -U || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG -pon || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG -poff || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG -p on || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG -p0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG -p 1 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --phantom 0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --phantom 1 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --phantom "off" || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --phantom "1" || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --phantom 0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG -LU --phantom 1 || exit $?
read -p "End of LOCK & PHANTOM. Proceed?"

### GAIN ###
# setup
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --unlock || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --manual || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --unmute || exit $?

# aliases
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 30 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 35.00 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --manual-gain 20 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG -g 10 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG -g15 || exit $?

# all possible values
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 0.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 0.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 1.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 1.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 2.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 2.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 3.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 3.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 4.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 4.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 5.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 5.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 6.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 6.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 7.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 7.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 8.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 8.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 9.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 9.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 10.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 10.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 11.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 11.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 12.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 12.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 13.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 13.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 14.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 14.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 15.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 15.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 16.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 16.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 17.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 17.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 18.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 18.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 19.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 19.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 20.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 20.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 21.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 21.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 22.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 22.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 23.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 23.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 24.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 24.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 25.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 25.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 26.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 26.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 27.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 27.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 28.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 28.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 29.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 29.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 30.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 30.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 31.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 31.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 32.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 32.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 33.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 33.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 34.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 34.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 35.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 35.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 36.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 36.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 37.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 37.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 38.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 38.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 39.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 39.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 40.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 40.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 41.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 41.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 42.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 42.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 43.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 43.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 44.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 44.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 45.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 45.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 46.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 46.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 47.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 47.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 48.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 48.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 49.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 49.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 50.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 50.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 51.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 51.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 52.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 52.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 53.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 53.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 54.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 54.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 55.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 55.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 56.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 56.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 57.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 57.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 58.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 58.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 59.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 59.5 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 60.0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 60.0 || exit $?

# setup
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 30 || exit $?

# expect fail
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 38.20 && echo 'passed, expected fail' && exit 9
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 64 && echo 'passed, expected fail' && exit 9
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --lock && echo 'passed, expected fail' && exit 9
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --gain 22 && echo 'passed, expected fail' && exit 9
read -p "End of GAIN. Proceed?"

### MIX ###
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --mix 0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --mix 1 || exit $?
# next two should fail
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --mix 2.5 && echo 'passed, expected fail' && exit 10
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --mix 4.6 && echo 'passed, expected fail' && exit 10
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --mix 10 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --mix 20 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --mix 30 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --mix 40 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --mix 50 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --mix 60 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --mix 70 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --mix 80 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --mix 90 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --mix 100 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --mix 40 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --only-playback || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --only-mic || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --only-usb || exit $?
read -p "End of MIX. Proceed?"

### MUTE ###
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --mute --mix 50 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --mix 10 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --mix 90 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --unmute --mix 10 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --mix 90 || exit $?

### AUTO ###
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --auto --auto-gain 1 --auto-tone 1 --auto-pos 0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --auto-gain 0 --auto-tone 0 --auto-pos 1 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --auto-gain 2 --auto-tone 2 --auto-pos 1 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --manual --gain 0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --mode auto || exit $?
read -p "End of MUTE and AUTO. Proceed?"

### MANUAL ###
# setup
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --manual --gain 40 || exit $?
# tests
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --compressor 0 --hpf 0 --limiter 0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --compressor 1 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --compressor 2 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --compressor 3 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --compressor off || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --compressor light || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --compressor medium || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --compressor heavy || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG -c 2 || exit $?
# not sure if spaceless versions work...
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG -c1 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --hpf 75 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --hpf 150 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --hpf off || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --hpf 75hz || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --hpf 150hz || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG -h 75 || exit $?
# ... especially -h, which is aliased to --help when no other arguments are given. Here, 0 is an argument!
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG -h0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --limiter 1 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --limiter off || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG --limiter on || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG -l 0 || exit $?
../shux/shux.py $WHEN_ACTIVE_DUMMY_FLAG -l1 || exit $?

# TODO: EQ
echo EQ is not yet implemented
