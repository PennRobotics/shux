from pytest  ## TODO TODO

# TODO: move to tests/
### # Check CRC-16 matches a known/captured data stream
### ex_data = bytes([0x11, 0x22, 0x9C, 0x03, 0x08, 0x08, 0x70, 0x08, 0x01, 0x02, 0x02, 0x00, 0x01, 0x04])  # noqa
### assert calc.checksum(ex_data) == 0xD2A4
### assert all(a == b for a, b in zip(ex_data[0:2], SHURE_HEADER_START))
### assert ex_data[4:5] == SHURE_HEADER_END
### assert ex_data[5:6] == ex_data[7:8]  # Data length
### assert ex_data[6:7] == SHURE_DATA_START
### assert all(a == b for a, b in zip(ex_data[8:11], CMD_GET_FEAT))
### assert all(a == b for a, b in zip(ex_data[12:14], Feat.MUTE.value))
### del ex_data

### assert CrcEm(tmp := b'\x00\x00\xcc\xdd\xee\xff') == tmp + b'\x8a\xa2'

### assert PadEm(bytes([5,6,7,8])) == b'\x05\x06\x07\x08\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0'

### assert HexEm(b'\x00\xff\x5a\x5a') == '00ff5a5a'

### assert ByteMe('00ff5a5a') == b'\x00\xff\x5a\x5a' + b'\x00'*59

