#!/bin/bash

RULES_PATH=/etc/udev/rules.d/62-shure-mvx2u.rules
if ! command -v virtualenv 2>&1 > /dev/null
then
	echo "virtualenv not found, exiting" >&2
	exit 2
fi

if ! test -f ${RULES_PATH}
then
	echo 'Generating udev rules'
	echo -e 'SUBSYSTEM=="hidraw", ATTRS{idVendor}=="14ed", ATTRS{idProduct}=="1013", MODE="0666"\nSUBSYSTEM=="usb", ATTRS{idVendor}=="14ed", ATTRS{idProduct}=="1013", MODE="0666"' | sudo tee ${RULES_PATH}
else
	echo 'Path already exists for udev rules'
fi

if ! test -d .env
then
	virtualenv .env && \
		source .env/bin/activate && \
		pip install -r requirements.txt
else
	echo 'A virtual environment already exists'
	# TODO: deal with broken requirements
fi
