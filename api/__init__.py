import asyncio
from asyncio import Future
from collections.abc import Iterable

from protocol import *


current_seq = 0

#TODO-debug
class MVDPort:
    def send(self, packet=None):
        pass
class MVD:
    def open(self, callback=None):
        return MVDPort()

mvdev = MVD()



class MVX2UApi:
    def __init__(self):
        self.futures = {}
        self.mv = mvdev.open(callback=self.__receive_packet)

    def __request(self, packet: MVX2UPacket):
        if packet.sequence_num in self.futures:
            return self.futures[sequence_num]
        loop = asyncio.get_running_loop()
        future = loop.create_future()
        self.futures[packet.sequence_num] = lambda pkt: loop.call_soon_threadsafe(future.set_result, pkt)
        self.mv.send(packet)
        return future

    def __request_param(self, param: Parameter):
        global current_seq
        current_seq += 1
        packet = MVX2UPacket(current_seq, PacketType.GET_PARAM_REQ, param.bytes())
        return self.__request(packet)

    def __receive_packet(self, b: bytes):
        packet = deserialize(b)
        if packet is not None:
            for pid in self.futures:
                if pid.matches(packet):
                    self.futures[pid](packet)
                    del self.futures[pid]
                    return
        print(self.futures.keys())
        print('received unknown packet: {0} / {1}'.format(b, packet))

    async def request_serial(self):
        # TODO
        pass

    async def request_version(self):
        # TODO
        pass

    async def request_48v_enabled(self):
        packet = await self.__request_param(Parameter.VOLTS)
        return bool(packet.data[4])

    # TODO: all of the others

    def __change_param(self, param: Parameter, param_val: Iterable[int]) -> Future[MVX2UPacket]:
        global current_seq
        current_seq += 1
        param_packet = MVX2UPacket(current_seq, PacketType.SET_PARAM_REQ, bytes(param.tuple(), *param_val))
        current_seq += 1
        ack_packet = MVX2UPacket(current_seq, PacketType.MAYBE_ACK_REQ, bytes((0,)))
        packet_futures = [self.__request(param_packet), self.__request(ack_packet)]
        return asyncio.gather(packet_futures)

    async def change_48v_enabled(self, enabled: bool) -> None:
        await self.__change_param(Parameter.VOLTS, [0x30 if enabled else 0x0])
