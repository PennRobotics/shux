from tkinter import *
from tkinter import ttk

PAD = 5
NARROW = 40

class DScale(ttk.Scale):
    _res_n = None

    def __init__(self, *args, **kwargs):
        self.resolution = kwargs.pop('resolution')
        self.chain = kwargs.pop('command', lambda *a: None)
        super().__init__(*args, command=self._var_chg, **kwargs)  # TODO: DScale or ttk.Scale?

    def _snap_to(self, e):
        self.set(self._res_n)

    def _var_chg(self, n):
        res_n = round(float(n) / self.resolution) * self.resolution
        if int(self.resolution) == self.resolution:
            res_n = int(res_n)
        self.setvar(self.cget('variable'), (res_n))
        self.chain(res_n)
        self._res_n = res_n

def cb(v):
    print('cb', v)

root = Tk()
root.title('Shure MVX2U Configuration')

content_holder = ttk.Frame(root)
panel_connect = ttk.Frame(content_holder, borderwidth=5, relief='ridge')
panel_connected = ttk.Frame(content_holder, borderwidth=5, relief='ridge')

label_status = ttk.Label(panel_connected, text='Status')
notebook = ttk.Notebook(panel_connected)
notebook.enable_traversal()
panel_auto = ttk.Frame(notebook, borderwidth=2)
panel_manual = ttk.Frame(notebook, borderwidth=2)

content_holder.grid(column=0, row=0, sticky='nsew')
panel_connect.grid(column=0, row=0, sticky='nsew')
panel_connected.grid(column=0, row=0, sticky='nsew')

notebook.grid(column=0, row=0, sticky='new')
label_status.grid(column=0, row=1, sticky='sew')

panel_auto.grid(column=0, row=0, sticky='nsew')

panel_manual.grid(column=0, row=0, sticky='nsew')

notebook.add(panel_auto, text='Auto')
notebook.add(panel_manual, text='Manual')

###

panel_connect.columnconfigure(0, weight=1)
panel_connect.rowconfigure(0, weight=1)
panel_connect.rowconfigure(1, weight=1)
ttk.Label(panel_connect, text='MVX2U adapter not detected\n\nPlug into USB port\nand check permissions', justify=CENTER).grid(column=0, row=0, sticky='s')
ttk.Button(panel_connect, text='Show config panes', command=panel_connected.tkraise).grid(column=0, row=1, sticky='n')

###

ttk.Label(panel_auto, text='Placeholder').grid()

###

_row = 0
val_phant = 0
ttk.Label(panel_manual, text='Phantom Power').grid(column=0, columnspan=2, row=_row, padx=PAD, pady=PAD, sticky='ew')
(scale_phant := DScale(panel_manual, from_=0, to=1, resolution=1, variable=val_phant, command=cb, length=NARROW, orient=HORIZONTAL)).grid(column=2, row=_row, padx=PAD, pady=PAD, sticky='e')
scale_phant.bind("<ButtonRelease>", scale_phant._snap_to, add='+')

_row = 1
ttk.Separator(panel_manual, orient=HORIZONTAL).grid(columnspan=3, row=_row, padx=0, pady=0, sticky='ew')

_row = 2
ttk.Label(panel_manual, text='Mic Mute').grid(column=0, columnspan=2, row=_row, padx=PAD, pady=PAD, sticky='ew')
(scale_mute := ttk.Scale(panel_manual, from_=0, to=1, length=NARROW, orient=HORIZONTAL)).grid(column=2, row=_row, padx=PAD, pady=PAD, sticky='e')

_row = 3
ttk.Separator(panel_manual, orient=HORIZONTAL).grid(columnspan=3, row=_row, padx=0, pady=0, sticky='ew')

_row = 4
val_gain = 0
ttk.Label(panel_manual, text='Mic Gain').grid(column=0, columnspan=2, row=_row, padx=PAD, pady=PAD, sticky='sw')
(label_gain := ttk.Label(panel_manual, textvariable=str(val_gain))).grid(column=2, row=_row, padx=PAD, pady=PAD, sticky='se')
_row = 5
(scale_gain := DScale(panel_manual, from_=0, to=60, resolution=0.5, variable=val_gain, command=cb, orient=HORIZONTAL)).grid(column=0, columnspan=3, row=_row, padx=PAD, pady=PAD, sticky='new')
scale_gain.bind("<ButtonRelease>", scale_gain._snap_to, add='+')

_row = 6
ttk.Separator(panel_manual, orient=HORIZONTAL).grid(columnspan=3, row=_row, padx=0, pady=0, sticky='ew')

_row = 7
ttk.Label(panel_manual, text='Monitor Mix').grid(column=0, columnspan=3, row=_row, padx=PAD, pady=PAD, sticky='s')
_row = 8
(scale_mix := ttk.Scale(panel_manual, from_=0, to=100, orient=HORIZONTAL)).grid(column=0, columnspan=3, row=_row, padx=PAD, pady=PAD, sticky='new')

_row = 9
ttk.Separator(panel_manual, orient=HORIZONTAL).grid(columnspan=3, row=_row, padx=0, pady=0, sticky='ew')

_row = 10
ttk.Label(panel_manual, text='Equalizer').grid(column=0, row=_row, padx=PAD, pady=PAD, sticky='ew')
(btn_eq_reset := ttk.Button(panel_manual, text='Reset')).grid(column=1, row=_row, padx=PAD, pady=PAD, sticky='e')
(scale_eq := ttk.Scale(panel_manual, from_=0, to=1, length=NARROW, orient=HORIZONTAL)).grid(column=2, row=_row, padx=PAD, pady=PAD, sticky='e')

_row = 11
eq_vals = []
(panel_eq := ttk.Frame(panel_manual)).grid(column=0, columnspan=3, row=_row, padx=PAD, pady=PAD, sticky='ew')
for col, (lbl, cb) in enumerate([('100', None),  # TODO: add callback
                                 ('250', None),
                                 ('1k',  None),
                                 ('4k',  None),
                                 ('10k', None)]):
    panel_eq.columnconfigure(col, weight=1)
    ttk.Label(panel_eq, text=lbl).grid(column=col, row=0)
    ttk.Scale(panel_eq, from_=-8, to=6, orient=VERTICAL).grid(column=col, row=1)
    (tmp_lbl := ttk.Label(panel_eq, text='+0')).grid(column=col, row=2)
    Checkbutton(panel_eq).grid(column=col, row=3)
    eq_vals.append(tmp_lbl)


_row = 12
ttk.Separator(panel_manual, orient=HORIZONTAL).grid(columnspan=3, row=_row, padx=0, pady=0, sticky='ew')

_row = 13
ttk.Label(panel_manual, text='High Pass Filter').grid(column=0, columnspan=3, row=_row, padx=PAD, pady=PAD, sticky='s')
_row = 14
(scale_hpf := ttk.Scale(panel_manual, from_=0, to=2, orient=HORIZONTAL)).grid(column=0, columnspan=3, row=_row, padx=PAD, pady=PAD, sticky='new')

_row = 15
ttk.Separator(panel_manual, orient=HORIZONTAL).grid(columnspan=3, row=_row, padx=0, pady=0, sticky='ew')

_row = 16
ttk.Label(panel_manual, text='Limiter').grid(column=0, columnspan=2, row=_row, padx=PAD, pady=PAD, sticky='ew')
(scale_lim := ttk.Scale(panel_manual, from_=0, to=1, length=NARROW, orient=HORIZONTAL)).grid(column=2, row=_row, padx=PAD, pady=PAD, sticky='e')

_row = 17
ttk.Separator(panel_manual, orient=HORIZONTAL).grid(columnspan=3, row=_row, padx=0, pady=0, sticky='ew')

_row = 18
ttk.Label(panel_manual, text='Compressor').grid(column=0, columnspan=3, row=_row, padx=PAD, pady=PAD, sticky='s')
_row = 19
(scale_comp := ttk.Scale(panel_manual, from_=0, to=3, orient=HORIZONTAL)).grid(column=0, columnspan=3, row=_row, padx=PAD, pady=PAD, sticky='new')

###

panel_connect.tkraise()

root.mainloop()

