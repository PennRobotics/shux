# This is a utility for viewing the microphone signal in a terminal.
# It is not very pretty but works on my PC/adapter combo.

DURATION = 15
SAMPLE_SIZE = 480
IS_16_BIT = False
WAVEFORM_MODE = True
SAMPLES_PER_LINE = 100  # waveform mode
OVERWRITE = False       # waveform mode
LINE_LEN = 200          # meter mode
EXPECTED_PEAK = 500000  # meter mode; true peak is almost 33000 in 16-bit, 8.4 million in 24-bit

import pyaudio
import struct
import sys
import time


def main():
    pai = pyaudio.PyAudio()

    idx = -1
    for i in range(pai.get_device_count()):
        if 'Shure' in pai.get_device_info_by_index(i)['name']:
            idx = i
            break
    if idx == -1:
        print('Mic not found')
        sys.exit(1)

    s = pai.open(input=True,
                 output=False,
                 format=(pyaudio.paInt16 if IS_16_BIT else pyaudio.paInt24),
                 channels=1,
                 rate=48000,
                 frames_per_buffer=SAMPLE_SIZE,
                 stream_callback=_cb,
                 input_device_index=idx)

    start = time.time()
    while s.is_active() and (time.time() - start) < DURATION:
        time.sleep(0.1)

    s.stop_stream()
    time.sleep(0.05)
    print('')

    s.close()
    pai.terminate()


cur = 100
def _cb(data_in, fc, ti, st):
    global cur

    # Throw away first sample (usually empty) and set pretty print counter
    if cur == 100:
        cur = 0 if WAVEFORM_MODE else 50
        return None, pyaudio.paContinue

    # Prepare data
    if IS_16_BIT:
        na = list(struct.iter_unpack('<h', bytes(data_in)))  # TODO-debug
    else:
        na = [(int.from_bytes(bytes(data_in[i:i+3]), 'little', signed=True),) for i in range(0, len(data_in), 3)]
    data_point = sum([abs(n[0]) for n in na]) // SAMPLE_SIZE

    if WAVEFORM_MODE:
        cur += 1

        K = 1 if IS_16_BIT else 0x40  # scaling constant
        if   data_point <     8*K: ch = ' '
        elif data_point <    25*K: ch = '\u2581'
        elif data_point <    80*K: ch = '\u2582'
        elif data_point <   250*K: ch = '\u2583'
        elif data_point <   800*K: ch = '\u2584'
        elif data_point <  2500*K: ch = '\u2585'
        elif data_point <  8000*K: ch = '\u2586'
        elif data_point < 25000*K: ch = '\u2587'
        elif data_point < 80000*K: ch = '\u2588'
        else:                      ch = 'x'

        print(ch, end='', flush=True)
        if cur % SAMPLES_PER_LINE == 0:
            cur = 0
            print('', end=('\r' if OVERWRITE else '\n'), flush=True)

    else:  # METER_MODE
        DTY = EXPECTED_PEAK // LINE_LEN
        n_fill = min(EXPECTED_PEAK, data_point) // DTY
        n_space = LINE_LEN - n_fill
        if cur < 50:  # (There is quite a bit dedicated to the peak indicator.)
            cur += 1
        if data_point > EXPECTED_PEAK:
            cur = 0
        print('\u2588'*(n_fill) + ' '*n_space, end=('\u2588'*3 + '\r' if cur < 50 else '   \r'), flush=True)

    return None, pyaudio.paContinue


if __name__ == '__main__':
    main()
