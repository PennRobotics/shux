// TODO: revise ALL measurements!
// TODO: measure depth and width of pins
// TODO: test 2d print
// TODO: test 3d print
// TODO: create and wire jig

module pad() {
  cylinder(2, d=0.925);
}

module drill() {
  cylinder(2, d=2.0);
  translate([0,0,0.16]) cylinder(0.2, d=4.5);
}

$fn=60;
TH=0.2;  /* For final print 3.0 */
EPS=0.001;

difference() {
  translate([0,0,EPS]) cube([37.0, 23.0, TH]);

  translate([ 2.0, 2.0]) drill();
  translate([31.0, 2.0]) drill();
  translate([31.0,21.0]) drill();
  translate([ 2.0,21.0]) drill();
  
  // Left Side
  translate([ 0.5, 5.5]) pad();  // #1
  translate([ 6.0,20.5]) pad();  // #2
  translate([ 7.5,12.5]) pad();  // gnd
  translate([ 2.5, 7.5]) pad();  // #8
  translate([ 5.0, 8.5]) pad();  // #9
  translate([ 4.5, 4.0]) pad();  // gnd
  translate([ 6.0, 6.0]) pad();  // #10
  translate([ 6.0, 2.0]) pad();  // J
  translate([ 8.5, 1.5]) pad();  // I
  translate([ 8.0, 4.2]) pad();  // K
  translate([10.5, 5.5]) pad();  // H
  translate([12.5, 7.0]) pad();  // G
  translate([15.0, 7.5]) pad();  // E
  translate([14.5,20.5]) pad();  // gnd TODO
  translate([16.5, 5.0]) pad();  // A

  translate([10.4, 2.2]) cube([6.4, 1.7, 2.0]);  // Debug port

  translate([16.5,6.5]) cube([7,7,2]);  // IC

  // Right Side
  translate([18.0, 1.5]) pad();  // C
  translate([19.0, 5.5]) pad();  // B
  translate([20.0, 2.5]) pad();  // D
  translate([22.0, 1.5]) pad();  // W
  translate([26.5,10.2]) pad();  // #4
  translate([26.5,15.5]) pad();  // #3
  translate([29.0, 4.0]) pad();  // Y
  translate([31.0,10.0]) pad();  // #6
  translate([32.0,13.0]) pad();  // #5
  translate([35.0, 5.3]) pad();  // Xbot
  translate([32.0, 5.3]) pad();  // gnd
  translate([35.0,17.0]) pad();  // #7
  translate([32.5,17.0]) pad();  // gnd
  translate([28.0,21.0]) pad();  // Xtop
  
  // Placement guides
  translate([ 0.0,11.5]) cylinder(5, d=5);  // light guide
  translate([ 2.0, 9.0]) cube([4, 6, 5]);  // inductor?
  translate([ 9.5,13]) cube([4.5,3,5]);  // yellow 1
  translate([ 9.5,19]) cube([4.5,5,5]);  // yellow 2
  translate([ 15,16]) cube([4,4,5]);  // IC
  translate([ 32.5,6.5]) cube([10, 10, 5]);  // USB
  // TODO: more as needed!  

}
