# Shure MVX2U Teardown

## Physical

### Printed Circuit Board

Notably populating the PCB on the Shure adapter:

#### Top

![front view of PCB with several ICs including an LPC55S69 and surface mount components](shure1.jpg "MVX2U Front"){width=480px}

![alternate front view of PCB with several ICs and surface mount components](shure2.jpg "MVX2U Front Angled"){width=480px}

- **NXP  LPC55S69**        &ndash; a dual-core Arm Cortex-M33 chip
- **TI   AIC3206**         &ndash; (may not be exact part num! **-Q1**?) stereo codec: DAC with 95 dB SNR to headphone (-70 THD+N), ADC with max 93 dB SNR (-84 THD+N typ.), DSP, 1.1 to 3.6 V (or 1.5 to 1.95, or up to 5.5, depending on pin), microphone input, headphone output, two gain amplifiers from 0 to +47.5 dB. There is a differential mode, so maybe that is what enables an input gain range of 0 to 60? There are even example scripts in the _Application Reference Guide_ for enabling an ADC in 48-kHz, high-performance mode.
- **TI   OPA2210**         &ndash; (or, the markings are vague) precision, low-power, 36 V op amp
- **TI   TLV320ADC5120**   &ndash; (or, the markings are vague) stereo ADC, 120 dB SNR
- _unknown 8 pin_          &ndash; Next to the USB-C connector is an 8-pin (looks like PowerPAK SO-8) with four legs (on the dot side) connected to a common trace. This looks an awful lot like an N-channel power MOSFET, and maybe the "27A" line in the three-line marking indicates 27 amps of drain current.
- _unknown 8? pin_         &ndash; Right on the D+ and D- lines of the USB-C connector is a chip etched with "C2U". What the heck? The only thing I can think/find is a CAT24C04, but that is for I2C and not USB-C.
- _unknown 8 pin_          &ndash; Marking "ABQ UEC 2004", looks by its pinout like it could be an MCP1725 LDO regulator.Pins 6 and 8 look a little inexact (resistor instead of capacitor, no visible Vout) but I doubt any of these smaller ICs are doing much else than signal manipulation or power regulation, so I'm not on a crusade to get the exact part number or functionality.
- _16 MHz crystal_         &ndash; steady external clock for the LPC
- _30-pin connector_       &ndash; looks almost like <s>Molex SlimStack</s> Panasonic AXF6J3012 (R35 connector), end pins are common ground, 0.35mm pitch (roughly measured)
- _test points_            &ndash; many test points, often without traces or connected to that slim-profile connector. These could also double as programming and measurement points using a custom bed of nails fixture
- _SMT transformer??_      &ndash; near the XLR connector, it sure looks like something bulky and wound, like a big inductor or transformer
- _green SMD cases_        &ndash; down near the transformer are a pair of components that look like a capacitor or resistor, except the leads look gold and the package is green and unmarked. Multi-Layer Ceramic Capacitors??
- _other stuff_            &ndash; looks like a lot of components for filtering, level shifting, buffering, etc.

As a commentary on vague chips: Some of the ICs look remarkably similar to one called the ["WTF Chip" on Reddit](https://www.reddit.com/r/ElectricalEngineering/comments/14l2se1/wtf_chip/). One of the replies suggested this could be a security product with intentionally misleading markings. I'm not so sure, as the communication sent via USB is not encrypted with the exception of the device firmware. I'd also suspect the built-in encryption engine of the NXP chip could handle anything needed for firmware. Then again, Shure has a remarkable history of imitation products. Seriously, do _not_ purchase an SM57 or SM58 or basically any other popular mic from Ebay. It's possible they threw in an authentication chip to dissuade all but the most dedicated counterfeiters. I doubt it, but it's been pretty tough to identify every chip on this board by markings alone.

![front view of PCB with annotations for test point connections](shure1-annot.jpg "MVX2U Front, Annotated"){width=900px}

![NXP LPC55S69 with non-GPIO pins colored](nxp-shure-pinmap.jpg "LPC55S69 Pinout")

| Annotation | Voltage | Description |
| ---        | ---     | ---         |
| 1          | ❔      | Possibly first rear LED connection |
| 2          | ❔      | Connects to corner pin of nearby IC |
| 3          | ❔      | ❔ _maybe LPC-connected_ |
| 4          | ❔      | ❔ _maybe LPC-connected_ |
| 5          | ❔      | ❔ _maybe USB-C-connected_ |
| 6          | ❔      | ❔ _maybe USB-C-connected_ |
| 7          | 3.1     | Possibly front LED connection |
| 8          | ❔      | Possibly second rear LED connection |
| 9          | ❔      | ❔ _maybe DSP IC related_ |
| 10         | ❔      | ❔ _maybe DSP IC related_ |
| A          | 1.9     | ❔ |
| B          | 1.9     | ❔ |
| C          | ❔      | ❔ |
| D          | 0.0     | ❔ |
| E          | ❔      | ❔ |
| F          | ❔      | ❔ |
| G          | ❔      | ❔ |
| H          | ❔      | ❔ |
| I          | ❔      | ❔ |
| J          | ❔      | ❔ |
| K          | ❔      | ❔ |
| L          | ❔      | ❔ |
| M          | ❔      | ❔ |
| N          | ❔      | ❔ |
| P          | ❔      | ❔ |
| Q          | ❔      | ❔ |
| R          | ❔      | ❔ |
| S          | ❔      | ❔ |
| T          | ❔      | ❔ |
| U          | ❔      | ❔ |
| V          | ❔      | ❔ |
| W          | ❔      | ❔ |
| X          | 5.0     | ❔ |
| Y          | 1.9     | ❔ |

#### Bottom

![back view of PCB with a few capacitors and wires visible](shure3.jpg "MVX2U Back"){width=480px}

- _big capacitors_         &ndash; a pair of smaller guys: probably 63 V and 2.2 uF; one bigger cap: looks like 63 V and _who knows_ how much capacitance, as the code is simply "10" and not "1R0" or "100".
- _other stuff_            &ndash; looks like a lot of components for signal filtering.

#### Daughter board

![all PCBs](shure4.jpg "MVX2U Back Stacked"){width=320px} ![HP jack PCB top](hp-top.jpg "HP PCB top"){width=320px}  ![HP jack PCB bottom](hp-bottom.jpg "HP PCB bottom"){width=320px}

#### Et cetera

Notably, one component to look for would be the phantom power. There is a [TI reference design](https://www.ti.com/tool/PMP40871) for this which uses a LDO voltage regulator, inductor, diode, and nearly a dozen capacitors&mdash;basically a subset of what is visible on the PCB. I'm willing to bet you can plug the device in with the case removed and start probing around and quickly pick out a test point with 48 volts coming off of a regulator near a diode on this board. (Refer to the diagram where the 48V points were identified. The bottom of the board seems like the high voltage side.)


## Firmware

### LPC55Sxx

A little bird told me this adapter is programmed using FreeRTOS.

Protection mechanisms in this chip:

- TrustZone
- secure boot ROM
- UUID (RFC4122)
- chip unique root key (PUF)
- PFR
- SHA engine
- AES256 engine
- SRAM PUF: keys generated at power-up
- PRINCE (encrypted flash)
- "CASPER"
- TRNG
- debug authentication
- intrusion prevention

There is a DEF CON presentation on [breaking TrustZone on the LPC55S69](https://www.youtube.com/watch?v=eKKgaGbcq4o), so unless there is an updated version of this chip or Shure has configured their device differently (e.g. set the lifecycle state appropriately, using on-the-fly encryption, locked down all other methods of access, etc.) there is the possibility we can plug right into the debug port and eventually dump the firmware.

I don't even necessarily care about the execution part of the firmware. Looking at the data section (strings, etc.) should provide a decent amount of insight into this device (e.g. if it also uses the "bootDSP C" command used by some other Motiv mics).

However, one end goal would be to figure out how the DSP is programmed in order to fine tune specific settings. Another would be to disable the headphone amp and to disable the audio sink for the headphone so that the device only works as a microphone input on smartphones and Linux computers and doesn't use extra power or produce extra signals on the output.


## Software

### Source

From what I can tell, there are some authors that were signing off on products in the comments not stripped from the Motiv Desktop executable. The main author, based in Hyderabad, now works someplace else. Perhaps the MVX2U hasn't been put in the Android or Apple versions of the Motiv app because someone new to these apps (the aforementioned author's replacement) must figure out how the MVX2U&mdash;which seems to differ from older models through its use of encryption and perhaps a different DSP chip&mdash;should connect using Android and Apple USB and encryption implementations. As an example, going from node-hid to two different USB HID libraries might be proving challenging. Any third-party implementation that doesn't reveal credentials or certificates used by Shure should be welcome! Right?

&hellip;or there are no plans to ever add the MVX2U to the smartphone apps (and, by corollary, not to Linux). That's not very nice IMHO.
