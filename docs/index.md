Shux Documentation
==================

**Shux** is a Python client for configuring the Shure MVX2U microphone adapter
that supports Linux.


<mark>THIS DOCUMENTATION NEEDS UPDATING</mark>

## MVX2U Users

### Quick Start

Plug in the adapter after loading the udev rules, or right before running the
Python script. There is a check which uses **lsusb** although this can be skipped
or removed after the device works. You will also need **git** (or else download
the **Shux** source manually) and **udev** and **pip** and, naturally, **python**.

```sh
git clone https://gitlab.com/PennRobotics/shux.git
cd shux
virtualenv --system-site-packages .env
source .env/bin/activate
pip install -r requirements.txt
echo -e 'SUBSYSTEM=="hidraw", ATTRS{idVendor}=="14ed", ATTRS{idProduct}=="1013", MODE="0666"\nSUBSYSTEM=="usb", ATTRS{idVendor}=="14ed", ATTRS{idProduct}=="1013", MODE="0666"' | sudo tee /etc/udev/rules.d/62-shure-mvx2u.rules
sudo udevadm control --reload-rules && sudo udevadm trigger
[[ $(lsusb -d 14ed:1013) ]] && echo "Ready" || echo "Connect MVX2U to a USB port"
cd shux
chmod +x shux.py
python ./shux.py
```

If there are problems installing the Python HID module, refer to the
[hidapi documentation](https://github.com/trezor/cython-hidapi).

It is possible that one of the udev rules is unnecessary. User group permissions
or system configuration might even make both udev rules unnecessary. On a Fedora
installation with few modifications, at least _hidraw_ was needed for using the
HID module. The USB rule was used during development as the adapter needed to be
reset and claimed each time the script was executed.

Set the script executable using `chmod +x shux.py`. You might also need to use
`python3` or some other executable name as the command, and you can change the
shebang to support a specific Python variant or version.

Suggestions for improving the _Quick Start_, documentation, and functionality are
vital to the success of this project! **Please report the first place where
friction is found while using this project.**


### Example syntax

View current configuration:

```sh
python ./shux.py
```

-----

Turn off phantom power:

```sh
./shux.py --unlock --phantom off
```

-----

See available flags:

```sh
./shux.py -h
```

_There are indeed more flags defined, such as --dummy, --apply-unchanged, --usb-in, --manual, --band3-gain, etc. These
  are mostly for troubleshooting, providing a clearer alternative to an existing flag, or&mdash;in the case of the EQ&mdash;
  for having a generic multi-argument flag that defines each specific class variable. This still has not been totally
  hammered out, and it is possible that ten specific flags are used in place of one (or two) generic flags. In any case,
  the hidden flags are easily discovered by searching for SUPPRESS in the source code._

-----

Set high-pass filter (HPF) to 75 Hz:

```sh
./shux.py -h 75hz
```

-----

Unlock device, showing raw messages:

```sh
./shux.py --unlock --debug
```

-----

A more advanced test case:

```sh
./shux.py --dummy --unlock --phantom on --limiter 0 --lock --apply-unchanged
```


### CLI

* `-L` / `--lock`
* `-U` / `--unlock`
* `--mode` (_auto_ or _manual_)
* `--auto-gain`(_quieter_, _normal_, _louder_; or _1_, _2_, _3_ maybe?)
* `--auto-position` (_near_ or _far_)
* `--auto-tone` (_dark_, _natural_, _bright_)
* `--playback` (0 &ndash; 100)
* `-c` / `--compressor` (_off_, _light_, _medium_, _heavy_; or 0 &&ndash; 3)
* `-e` / `--eq` (_off_, _on_; _0_, _1_)
* `-h` / `--hpf` (_off_, _75hz_, _150hz_; 0 &ndash; 2)
* `-h` without arguments will display the help message; also, `-?` or `--help`
* `-l` / `--limiter` (_off_, _on_; _0_, _1_)
* `-g` / `--gain` (manual gain: 0.0 &ndash; 60.0, in increments of 0.5)
* `-m` / `--mute` (_off_, _on_; _0_, _1_; default is _on_, which means "muted")
* `-p` / `--phantom` (_off_, _on_; _0_, _1_/_48V_; default is _on_)
* `--reset-eq`
* `--unmute`
* `-v` / `--version`
* `-q` / `--quiet` (use twice for "extra quiet")
* `--debug`

It is possible that these flags and acceptable values change over time. When in doubt, use
the `--help` flag or check the **argparse** sections of the source code.

The `--eqband` setting needs some thought, since there needs to be a clean, clear, concise way to
define whether each band is active as well as the specific gain for that band. One possibility is
having suboptions, like `--eqband 100hz off`, `--eqband 100hz +2`, `--eqband 100 2` but then it
becomes unclear if "off" and 0 should do the same thing. This is also just very verbose, although
it gives you the granularity to keep the previous setting. Another is having the enable and gain
amounts as two separate flags with a comma-separated list: `--eq-bands 0,0,1,0,1`,
`--eq-gains +4,-2,0,2,+2`. Trying to combine it any more than this seems foolish and complicated.
One thing to decide is how exactly to write out each frequency or band.
The easiest is to just state `--eqband 1 off +2`, `--eqband 2 on +4`, `--eqband 3 on +2`, etc.
and then each term after the band number can be optional, but the first would only be between _on_
and _off_ while the second would be any signed integer with optional positive sign. That seems to
make the most sense!


### WebHID

The WebHID effort has its own repository: [Shhid](https://gitlab.com/PennRobotics/shhid)


### GUI

#### Python
A _tkinter_-based GUI is being developed in **walls.py**.

#### Android
<mark> TODO </mark>

_With the accomplishment of getting WebHID working, I want to see if it is reasonable to connect to a USB device (other than a mouse, keyboard, audio, or video device) in Kotlin. If so, I think we will be able to program the mic adapter with a smartphone._



## Developers

### Current Status

- View current adapter configuration
- Send/receive raw data asynchronously
- tested but not well tested
- works in Linux and Windows
- Unlock/lock adapter
- Change most feature settings (one notable exception are EQ adjustments, although others might be broken!)
- Nonstandard packet types (e.g. false commands, unlisted features, unacceptable value, attempting to change a locked setting) are not fully known
- GUI is partially laid out but no callbacks are defined

_Refer to the repository issues/milestones for unfinished business._


### Structure

- [main]
    - **LICENSE**
    - **README.md**, the quick start guide
    - **requirements.txt**, Pip modules needed for the main program
    - **.gitignore**
    - docs/
        - **index.md**, this document
    - etc/
        - **livedisp.py**, for displaying crude audio endpoint I/O
        - **requirements.txt**, Pip modules needed for **livedisp.py**
        - **walls.py**, work-in-progress GUI
        - re/, contains files supporting adapter reverse engineering
    - shux/
        - \_**version**
        - **shux.py**, the main program
        - classes/
            - **decoder.py**
            - **dummyadapter.py**
            - **features.py**
            - **mvparser.py**
            - **mvx2u.py**
            - **subscriber.py**
    - tests/
        - **clitest.sh**, manual test of nearly all settings


### Tests

There is currently a shell script for manually entering each of the CLI flags
as well as combinations of flags. There is also a dummy adapter which should
generate placeholder packet data&mdash;mostly zeroes in response to commands.
Inline asserts should ensure the correct packet structure/types are used.


### Related tools

- For configuring the MV7 in Linux? https://github.com/matteodelabre/mv7config



## Background

Shure only publishes a desktop application for Windows and Mac. I was not able to get Wine to access the USB adapter. After capturing some USB packets in Windows, I decided to create a Linux script so I could use my MVX2U without Windows.

[Snippet 2595361](https://gitlab.com/-/snippets/2595361) has the to-date details of how the [Shure MVX2U](https://www.shure.com/en-US/products/accessories/mvx2u_xlr_usb_interface) communicates with its configuration software, _ShurePlus MOTIV™ Desktop_.

This repository's [wiki](https://gitlab.com/PennRobotics/shux/-/wikis/home) has additional information from a short USB capture session. The MVX2U was plugged in with the desktop application already open and waiting, then the unlock toggle was clicked after about 7 seconds. Several seconds later (probably 7 more) the device was locked again. After about 20 seconds, the capture ended.

