## Done

- Display most settings
- Read from MVX2U
- Simulate an MVX2U if none is detected


## TODO

- Write to MVX2U
- Read audio levels during configuration
- Read EQ bands
- Refer to inline TODO tags

