# TODO-lo: remove variables for ttk elements not needing interaction (static labels, etc)
# TODO-hi: prefer a single unit for widths e.g. pixels
# TODO-lo: fix takefocus when each panel is not visible
# TODO-lo: create a ttkbootstrap branch
# TODO-lo: use Ttk styles in a dark-theme branch
# TODO: add send/receive indicators
# TODO: add Lock
# TODO: move all the classes and crap into here!!
from enum import IntEnum, auto
from functools import partial
import tkinter as tk
import tkinter.ttk as ttk

from classes.mvx2u import *


VOL_RATE_HZ = 16  # TODO: implement
GUI_RATE_HZ = 4  # TODO: implement

class MVX2USetting(IntEnum):
    PHANTOM = auto()
    MIC_MUTE = auto()
    PLAY_MIX = auto()
    AUTO_MODE = auto()
    AUTO_DIST = auto()
    AUTO_TONE = auto()
    AUTO_GAIN = auto()
    MAN_GAIN = auto()
    MAN_EQ_EN = auto()
    MAN_100HZ_EN = auto()
    MAN_100HZ_DB = auto()
    MAN_250HZ_EN = auto()
    MAN_250HZ_DB = auto()
    MAN_1KHZ_EN = auto()
    MAN_1KHZ_DB = auto()
    MAN_4KHZ_EN = auto()
    MAN_4KHZ_DB = auto()
    MAN_10KHZ_EN = auto()
    MAN_10KHZ_DB = auto()
    MAN_HPF = auto()
    MAN_LIMIT = auto()
    MAN_COMP = auto()
    SN = auto()
    HP_VOL = auto()


class ShuxApp(tk.Tk):
    dev_connected = False

    def update_setting(self, setting, *va, **kwa):
        assert isinstance(setting, MVX2USetting), "setting should be an Enum"
        match setting:
            # TODO-hi: call the appropriate class fn (for all cases)
            case MVX2USetting.PHANTOM:
                print(" >> phantom")
            case MVX2USetting.MIC_MUTE:
                print(" >> mic mute")
            case MVX2USetting.PLAY_MIX:
                print(" >> play mix")
            case MVX2USetting.AUTO_MODE:
                print(" >> auto/manual mode")
            case MVX2USetting.AUTO_DIST:
                print(" >> auto dist")
            case MVX2USetting.AUTO_TONE:
                print(" >> auto tone")
            case MVX2USetting.AUTO_GAIN:
                print(" >> auto gain")
            case MVX2USetting.MAN_GAIN:
                print(" >> db gain")
            case MVX2USetting.MAN_EQ_EN:
                print(" >> eq enable")
            case MVX2USetting.MAN_100HZ_EN:
                print(" >> 100hz en")
            case MVX2USetting.MAN_100HZ_DB:
                print(" >> 100hz db")
            case MVX2USetting.MAN_250HZ_EN:
                print(" >> 250hz en")
            case MVX2USetting.MAN_250HZ_DB:
                print(" >> 250hz db")
            case MVX2USetting.MAN_1KHZ_EN:
                print(" >> 1khz en")
            case MVX2USetting.MAN_1KHZ_DB:
                print(" >> 1khz db")
            case MVX2USetting.MAN_4KHZ_EN:
                print(" >> 4khz en")
            case MVX2USetting.MAN_4KHZ_DB:
                print(" >> 4khz db")
            case MVX2USetting.MAN_10KHZ_EN:
                print(" >> 10khz en")
            case MVX2USetting.MAN_10KHZ_DB:
                print(" >> 10khz db")
            case MVX2USetting.MAN_HPF:
                print(" >> hpf")
            case MVX2USetting.MAN_LIMIT:
                print(" >> limit")
            case MVX2USetting.MAN_COMP:
                print(" >> comp")
            case MVX2USetting.SN:
                print(" >> serial")
            case MVX2USetting.HP_VOL:
                print(" >> hp vol")
        self.update_label(setting)

    # TODO: remove function
    def dump_vars(self, *e):
        print(" VARS:")
        print(f"phantom: {self.phantom.get()}")
        print(f"mute: {self.mute.get()}")
        print(f"playback_mix: {self.playback_mix.get()}")
        print(f"frame_is_auto: {self.frame_is_auto.get()}")
        print("  AUTO")
        print(f"auto_mic_distance: {self.auto_mic_distance.get()}")
        print(f"auto_tone: {self.auto_tone.get()}")
        print(f"auto_gain: {self.auto_gain.get()}")
        print("  MANUAL")
        print(f"manual_gain: {self.manual_gain_x2.get() / 2}")
        print(f"manual_eq_enabled: {self.manual_eq_enabled.get()}")
        print(f"100hz band {'enabled' if self.manual_eq_100hz_en.get() else 'disabled'}", end="")
        print(f", db: {self.manual_eq_100hz_db.get()}")
        print(f"250hz band {'enabled' if self.manual_eq_250hz_en.get() else 'disabled'}", end="")
        print(f", db: {self.manual_eq_250hz_db.get()}")
        print(f"1khz band {'enabled' if self.manual_eq_1khz_en.get() else 'disabled'}", end="")
        print(f", db: {self.manual_eq_1khz_db.get()}")
        print(f"4khz band {'enabled' if self.manual_eq_4khz_en.get() else 'disabled'}", end="")
        print(f", db: {self.manual_eq_4khz_db.get()}")
        print(f"10khz band {'enabled' if self.manual_eq_10khz_en.get() else 'disabled'}", end="")
        print(f", db: {self.manual_eq_10khz_db.get()}")
        print(f"manual_hpf: {self.manual_hpf.get()}")
        print(f"manual_limiter: {self.manual_limiter.get()}")
        print(f"manual_comp: {self.manual_comp.get()}")
        print(f"serial_num: {self.serial_num.get()}")
        print(f"Mic volume: {self.volume_mic.get()}")
        print(f"HP volume: {self.volume_hp.get()}")
        if e:
            print("---")
            print(*e)
        print("===")

    def set_active_panel(self, panel):
        match panel:
            case "auto":
                self.frame_is_auto.set(True),
                self.auto_frame.tkraise(),
                self.auto_button.state(["pressed"]),
                self.manual_button.state(["!pressed"]),
                self.update_setting(MVX2USetting.AUTO_MODE),
            case "manual":
                self.frame_is_auto.set(False),
                self.manual_frame.tkraise(),
                self.auto_button.state(["!pressed"]),
                self.manual_button.state(["pressed"]),
                self.update_setting(MVX2USetting.AUTO_MODE),
            case _:
                assert False, "argument should be 'auto' or 'manual'"

    def get_elements_from_device(self, startup=False):
        if startup:
            self.phantom.set(False)
            self.mute.set(False)
            self.playback_mix.set(0)
            self.frame_is_auto.set(True)
            self.auto_mic_distance.set("Near")
            self.auto_tone.set("Normal")
            self.auto_gain.set("Normal")
            self.manual_gain_x2.set(0)
            self.manual_eq_enabled.set(False)
            self.manual_eq_100hz_en.set(False)
            self.manual_eq_100hz_db.set(0)
            self.manual_eq_250hz_en.set(False)
            self.manual_eq_250hz_db.set(0)
            self.manual_eq_1khz_en.set(False)
            self.manual_eq_1khz_db.set(0)
            self.manual_eq_4khz_en.set(False)
            self.manual_eq_4khz_db.set(0)
            self.manual_eq_10khz_en.set(False)
            self.manual_eq_10khz_db.set(0)
            self.manual_hpf.set("Off")
            self.manual_limiter.set(False)
            self.manual_comp.set("Off")
            self.serial_num.set("")
            self.volume_mic.set(0.0)
            self.volume_hp.set(0.0)

            self.update_label()
            return

        try:
            self.mv = ShureMVX2U()
            DUMMY_DEVICE = False
        except ConnectionError:
            DUMMY_DEVICE = True

        self.dev_connected = True

        if DUMMY_DEVICE:
            self.phantom.set(True)
            self.mute.set(False)
            self.playback_mix.set(30)
            self.frame_is_auto.set(False)
            self.auto_mic_distance.set("Far")
            self.auto_tone.set("Dark")
            self.auto_gain.set("Loud")
            self.manual_gain_x2.set(42.5 * 2)
            self.manual_eq_enabled.set(True)
            self.manual_eq_100hz_en.set(True)
            self.manual_eq_100hz_db.set(-4)
            self.manual_eq_250hz_en.set(False)
            self.manual_eq_250hz_db.set(-2)
            self.manual_eq_1khz_en.set(True)
            self.manual_eq_1khz_db.set(0)
            self.manual_eq_4khz_en.set(False)
            self.manual_eq_4khz_db.set(2.5)
            self.manual_eq_10khz_en.set(True)
            self.manual_eq_10khz_db.set(6)
            self.manual_hpf.set("150 Hz")
            self.manual_limiter.set(True)
            self.manual_comp.set("Lo")
            self.serial_num.set("Dummy Device")
            self.volume_mic.set(20.5)
            self.volume_hp.set(63.3)
        else:
            self.mv.read_config()

            self.phantom.set(self.mv._phantomPowerVolts > 0)
            self.mute.set(self.mv._micMute)
            self.playback_mix.set(self.mv._monitorMix)
            self.frame_is_auto.set(self.mv._autoLevel)
            match self.mv._autoLevelPosition:
                case 0:
                    self.auto_mic_distance.set("Near")
                case 1:
                    self.auto_mic_distance.set("Far")
                case _:
                    assert False, "Unknown auto distance setting"
            match self.mv._autoLevelTone:
                case 0:
                    self.auto_tone.set("Dark")
                case 1:
                    self.auto_tone.set("Normal")
                case 2:
                    self.auto_tone.set("Bright")
                case _:
                    assert False, "Unknown auto tone setting"
            match self.mv._autoLevelConfig:
                case 0:
                    self.auto_gain.set("Quiet")
                case 1:
                    self.auto_gain.set("Normal")
                case 2:
                    self.auto_gain.set("Loud")
                case _:
                    assert False, "Unknown auto gain setting"
            self.manual_gain_x2.set(self.mv._inputGain / 50)
            self.manual_eq_enabled.set(self.mv._peqEnabled)
            # TODO: self.manual_eq_100hz_en.set(True)
            # TODO: self.manual_eq_100hz_db.set(-4)
            # TODO: self.manual_eq_250hz_en.set(False)
            # TODO: self.manual_eq_250hz_db.set(-2)
            # TODO: self.manual_eq_1khz_en.set(True)
            # TODO: self.manual_eq_1khz_db.set(0)
            # TODO: self.manual_eq_4khz_en.set(False)
            # TODO: self.manual_eq_4khz_db.set(2.5)
            # TODO: self.manual_eq_10khz_en.set(True)
            # TODO: self.manual_eq_10khz_db.set(6)
            match self.mv._highPassFilterMode:
                case 0:
                    self.manual_hpf.set("Off")
                case 1:
                    self.manual_hpf.set("75 Hz")
                case 2:
                    self.manual_hpf.set("150 Hz")
                case _:
                    assert False, "Unknown HPF setting"
            print("DEBUG: ", type(self.mv._limiterSelect))  # TODO-debug
            # self.manual_limiter.set(self.mv._limiterSelect)
            match self.mv._compressorMode:
                case 0:
                    self.manual_comp.set("Off")
                case 1:
                    self.manual_comp.set("Lo")
                case 2:
                    self.manual_comp.set("Mid")
                case 3:
                    self.manual_comp.set("Hi")
            print("DEBUG: ", self.mv.mv_dev.get_product_string())  # TODO-debug
            #self.serial_num.set("123DUMMY56")  # TODO: use correct function (see prev line)
            #self.volume_mic.set(0.0)
            #self.volume_hp.set(0.0)

        self.set_active_panel("auto" if self.frame_is_auto.get() else "manual")
        self.update_label()

    def update_label(self, label=None):
        assert label is None or isinstance(label, MVX2USetting), "label should be an Enum"

        if not label or label == MVX2USetting.PLAY_MIX:
            mix_mic = min(100, 200 - 2 * self.playback_mix.get())
            self.mix_mic_value.config(text=f"{mix_mic} %")

            mix_play = min(100, 2 * self.playback_mix.get())
            self.mix_play_value.config(text=f"{mix_play} %")

        if not label or label == MVX2USetting.MAN_GAIN:
            gain = self.manual_gain_x2.get() / 2
            self.manual_gain_label.config(text=f"Gain: {gain:.1f} dB")

        if not label or label == MVX2USetting.MAN_100HZ_DB:
            db = (self.manual_eq_100hz_db.get() // 2) * 2
            self.manual_eq_100_val.config(text=f"{db:+}")
        if not label or label == MVX2USetting.MAN_250HZ_DB:
            db = (self.manual_eq_250hz_db.get() // 2) * 2
            self.manual_eq_250_val.config(text=f"{db:+}")
        if not label or label == MVX2USetting.MAN_1KHZ_DB:
            db = (self.manual_eq_1khz_db.get() // 2) * 2
            self.manual_eq_1000_val.config(text=f"{db:+}")
        if not label or label == MVX2USetting.MAN_4KHZ_DB:
            db = (self.manual_eq_4khz_db.get() // 2) * 2
            self.manual_eq_4000_val.config(text=f"{db:+}")
        if not label or label == MVX2USetting.MAN_10KHZ_DB:
            db = (self.manual_eq_10khz_db.get() // 2) * 2
            self.manual_eq_10000_val.config(text=f"{db:+}")

        if not label or label == MVX2USetting.SN:
            match self.dev_connected:
                case False:
                    self.status_label.config(text="No adapter connected")
                case True:
                    try:
                        self.mv
                        self.status_label.config(text=f"Connected, S/N {self.serial_num.get()}")
                    except AttributeError:
                        self.status_label.config(text=f"Using a simulated, read-only adapter")

    def __init__(self):
        super().__init__()

        # Window
        self.title("Shux – MVX2U Linux Client")
        self.geometry("420x680")
        self.resizable(False, False)

        # Variables
        self.phantom = tk.BooleanVar(self)
        self.mute = tk.BooleanVar(self)
        self.playback_mix = tk.IntVar(self)

        self.frame_is_auto = tk.BooleanVar(self)  # TODO

        self.auto_mic_distance = tk.StringVar(self)
        self.auto_tone = tk.StringVar(self)
        self.auto_gain = tk.StringVar(self)

        self.manual_gain_x2 = tk.IntVar(self)
        self.manual_eq_enabled = tk.BooleanVar(self)
        self.manual_eq_100hz_en = tk.BooleanVar(self)
        self.manual_eq_100hz_db = tk.IntVar(self)
        self.manual_eq_250hz_en = tk.BooleanVar(self)
        self.manual_eq_250hz_db = tk.IntVar(self)
        self.manual_eq_1khz_en = tk.BooleanVar(self)
        self.manual_eq_1khz_db = tk.IntVar(self)
        self.manual_eq_4khz_en = tk.BooleanVar(self)
        self.manual_eq_4khz_db = tk.IntVar(self)
        self.manual_eq_10khz_en = tk.BooleanVar(self)
        self.manual_eq_10khz_db = tk.IntVar(self)
        self.manual_hpf = tk.StringVar(self)
        self.manual_limiter = tk.BooleanVar(self)
        self.manual_comp = tk.StringVar(self)

        self.serial_num = tk.StringVar(self)
        self.volume_mic = tk.DoubleVar(self)
        self.volume_hp = tk.DoubleVar(self)

        # UI Elements
        # - Common
        self.top_frame = ttk.Frame(self)
        self.top_frame.pack(fill=tk.X)

        self.control_row = ttk.Frame(self.top_frame)
        ttk.Button(
                self.control_row,
                text="Scan",
                padding="-5 10 -5 10",
                command=lambda: print("TODO: scan"),
                ).pack(side=tk.LEFT, padx=8)
        # TODO-lo: switch to "Disconnect" after connection
        ttk.Button(
                self.control_row,
                text="Connect",
                padding="-5 10 -5 10",
                command=partial(self.get_elements_from_device),
                ).pack(side=tk.LEFT, padx=8)
        ttk.Button(
                self.control_row,
                text="Exit",
                padding="-5 10 -5 10",
                command=lambda: self.quit(),
                ).pack(side=tk.LEFT, padx=8)
        self.control_row.pack(pady=8)

        ttk.Separator(self.top_frame, orient=tk.HORIZONTAL).pack(pady=8, fill=tk.X, expand=True)

        self.phantom_row = ttk.Frame(self.top_frame)
        ttk.Label(self.phantom_row, text="Phantom Power:", width=20).pack(side=tk.LEFT)
        ttk.Radiobutton(
                self.phantom_row,
                text="48 V",
                variable=self.phantom,
                value=True,
                width=10,
                command=partial(self.update_setting, MVX2USetting.PHANTOM),
                ).pack(side=tk.LEFT)
        ttk.Radiobutton(
                self.phantom_row,
                text="Off",
                variable=self.phantom,
                value=False,
                width=10,
                command=partial(self.update_setting, MVX2USetting.PHANTOM),
                ).pack(side=tk.LEFT)
        self.phantom_row.pack(pady=8)

        self.mute_row = ttk.Frame(self.top_frame)
        ttk.Label(self.mute_row, text="Microphone:", width=20).pack(side=tk.LEFT)
        ttk.Radiobutton(
                self.mute_row,
                text="Muted",
                variable=self.mute,
                value=True,
                width=10,
                command=partial(self.update_setting, MVX2USetting.MIC_MUTE),
                ).pack(side=tk.LEFT)
        ttk.Radiobutton(
                self.mute_row,
                text="Unmuted",
                variable=self.mute,
                value=False,
                width=10,
                command=partial(self.update_setting, MVX2USetting.MIC_MUTE),
                ).pack(side=tk.LEFT)
        self.mute_row.pack(pady=8)

        self.mix_row = ttk.Frame(self.top_frame)
        self.mix_label_row = ttk.Frame(self.mix_row)
        # TODO-lo: fix label spacing/padding
        ttk.Label(self.mix_label_row, text=" Mic", width=10, anchor=tk.W).pack(side=tk.LEFT, fill=tk.X)
        ttk.Label(self.mix_label_row, text="Playback ", width=10, anchor=tk.E).pack(side=tk.RIGHT, fill=tk.X)
        ttk.Label(self.mix_label_row, text=" Mix:", anchor=tk.CENTER).pack(expand=True)
        self.mix_label_row.pack(fill=tk.X)
        self.mix_mic_value = ttk.Label(
                self.mix_row,
                text="100%",
                width=5,
                anchor=tk.CENTER,
                )
        self.mix_mic_value.pack(side=tk.LEFT)
        self.mix_scale = ttk.Scale(
                self.mix_row,
                variable=self.playback_mix,
                from_=0,
                to=100,
                command=partial(self.update_setting, MVX2USetting.PLAY_MIX),
                )
        self.mix_scale.pack(side=tk.LEFT, fill=tk.X, expand=True)
        self.mix_play_value = ttk.Label(
                self.mix_row,
                text="0%",
                width=5,
                anchor=tk.CENTER,
                )
        self.mix_play_value.pack(side=tk.LEFT)
        self.mix_row.pack(pady=8, fill=tk.X, expand=True)

        # TODO: differentiate between HP volume and input (mic) signal
        self.volume_row = ttk.Frame(self.top_frame)
        self.volume_label_row = ttk.Frame(self.volume_row)
        ttk.Label(self.volume_label_row, text="Mic Vol:", anchor=tk.CENTER).pack(side=tk.LEFT, expand=True)
        ttk.Label(self.volume_label_row, text="Jack Vol:", anchor=tk.CENTER).pack(side=tk.RIGHT, expand=True)
        self.volume_label_row.pack(fill=tk.X, expand=True)
        self.volume_mic_meter = ttk.Progressbar(
                self.volume_row,
                variable=self.volume_mic,
                # TODO-lo: define the rest of this element
                )
        self.volume_mic_meter.pack(side=tk.LEFT, padx=8, fill=tk.X, expand=True)
        self.volume_hp_meter = ttk.Progressbar(
                self.volume_row,
                variable=self.volume_hp,
                # TODO-lo: define the rest of this element
                )
        self.volume_hp_meter.pack(side=tk.RIGHT, padx=8, fill=tk.X, expand=True)
        self.volume_row.pack(pady=8, fill=tk.X, expand=True)

        ttk.Separator(self.top_frame, orient=tk.HORIZONTAL).pack(pady=8, fill=tk.X, expand=True)

        # - Auto / Manual Visibility
        self.selector_row = ttk.Frame(self.top_frame)
        self.auto_button = ttk.Button(
                self.selector_row,
                text="Auto",
                command=partial(self.set_active_panel, "auto"),
                )
        self.auto_button.pack(side=tk.LEFT, padx=40, pady=8)
        self.manual_button = ttk.Button(
                self.selector_row,
                text="Manual",
                command=partial(self.set_active_panel, "manual"),
                )
        self.manual_button.pack(side=tk.RIGHT, padx=40, pady=8)
        self.selector_row.pack(pady=8, fill=tk.X)

        ttk.Separator(self.top_frame, orient=tk.HORIZONTAL).pack(pady=8, fill=tk.X, expand=True)

        self.center_container = ttk.Frame(self)
        self.center_container.pack()
        self.center_container.grid_rowconfigure(0, weight=1)
        self.center_container.grid_columnconfigure(0, weight=1)

        self.auto_frame = ttk.Frame(self.center_container)
        self.auto_frame.grid(row=0, column=0, sticky="nsew")

        self.manual_frame = ttk.Frame(self.center_container)
        self.manual_frame.grid(row=0, column=0, sticky="nsew")

        # - Auto
        self.auto_dist_row = ttk.Frame(self.auto_frame)
        ttk.Label(self.auto_dist_row, text="Distance:", width=12).pack(side=tk.LEFT)
        ttk.Radiobutton(
                self.auto_dist_row,
                variable=self.auto_mic_distance,
                value="Near",
                text="Near",
                width=12,
                command=partial(self.update_setting, MVX2USetting.AUTO_DIST),
                ).pack(side=tk.LEFT)
        ttk.Radiobutton(
                self.auto_dist_row,
                variable=self.auto_mic_distance,
                value="Far",
                text="Far",
                width=12,
                command=partial(self.update_setting, MVX2USetting.AUTO_DIST),
                ).pack(side=tk.LEFT)
        self.auto_dist_row.pack(pady=8, fill=tk.X)

        self.auto_tone_row = ttk.Frame(self.auto_frame)
        ttk.Label(self.auto_tone_row, text="Tone:", width=10).pack(side=tk.LEFT)
        ttk.Radiobutton(
                self.auto_tone_row,
                variable=self.auto_tone,
                value="Dark",
                text="Dark",
                width=8,
                command=partial(self.update_setting, MVX2USetting.AUTO_TONE),
                ).pack(side=tk.LEFT)
        ttk.Radiobutton(
                self.auto_tone_row,
                variable=self.auto_tone,
                value="Normal",
                text="Normal",
                width=8,
                command=partial(self.update_setting, MVX2USetting.AUTO_TONE),
                ).pack(side=tk.LEFT)
        ttk.Radiobutton(
                self.auto_tone_row,
                variable=self.auto_tone,
                value="Bright",
                text="Bright",
                width=8,
                command=partial(self.update_setting, MVX2USetting.AUTO_TONE),
                ).pack(side=tk.LEFT)
        self.auto_tone_row.pack(pady=8, fill=tk.X)

        self.auto_gain_row = ttk.Frame(self.auto_frame)
        ttk.Label(self.auto_gain_row, text="Gain:", width=10).pack(side=tk.LEFT)
        ttk.Radiobutton(
                self.auto_gain_row,
                variable=self.auto_gain,
                value="Quiet",
                text="Quiet",
                width=8,
                command=partial(self.update_setting, MVX2USetting.AUTO_GAIN),
                ).pack(side=tk.LEFT)
        ttk.Radiobutton(
                self.auto_gain_row,
                variable=self.auto_gain,
                value="Normal",
                text="Normal",
                width=8,
                command=partial(self.update_setting, MVX2USetting.AUTO_GAIN),
                ).pack(side=tk.LEFT)
        ttk.Radiobutton(
                self.auto_gain_row,
                variable=self.auto_gain,
                value="Loud",
                text="Loud",
                width=8,
                command=partial(self.update_setting, MVX2USetting.AUTO_GAIN),
                ).pack(side=tk.LEFT)
        self.auto_gain_row.pack(pady=8, fill=tk.X)

        # - Manual
        self.manual_gain_row = ttk.Frame(self.manual_frame)
        self.manual_gain_label = ttk.Label(self.manual_gain_row, text="Gain:  0.0 dB", width=13)
        self.manual_gain_label.pack(side=tk.LEFT)
        ttk.Scale(
                self.manual_gain_row,
                variable=self.manual_gain_x2,
                from_=0,
                to=120,
                length=244,
                command=partial(self.update_setting, MVX2USetting.MAN_GAIN),
                ).pack(side=tk.LEFT)
        self.manual_gain_row.pack(pady=3, fill=tk.X)

        self.manual_comp_row = ttk.Frame(self.manual_frame)
        ttk.Label(self.manual_comp_row, text="Compression:", width=13).pack(side=tk.LEFT)
        ttk.Radiobutton(
                self.manual_comp_row,
                variable=self.manual_comp,
                value="Off",
                text="Off",
                width=6,
                command=partial(self.update_setting, MVX2USetting.MAN_COMP),
                ).pack(side=tk.LEFT)
        ttk.Radiobutton(
                self.manual_comp_row,
                variable=self.manual_comp,
                value="Lo",
                text="Lo",
                width=6,
                command=partial(self.update_setting, MVX2USetting.MAN_COMP),
                ).pack(side=tk.LEFT)
        ttk.Radiobutton(
                self.manual_comp_row,
                variable=self.manual_comp,
                value="Mid",
                text="Mid",
                width=6,
                command=partial(self.update_setting, MVX2USetting.MAN_COMP),
                ).pack(side=tk.LEFT)
        ttk.Radiobutton(
                self.manual_comp_row,
                variable=self.manual_comp,
                value="Hi",
                text="Hi",
                width=6,
                command=partial(self.update_setting, MVX2USetting.MAN_COMP),
                ).pack(side=tk.LEFT)
        self.manual_comp_row.pack(pady=3, fill=tk.X)

        self.manual_lim_row = ttk.Frame(self.manual_frame)
        ttk.Label(self.manual_lim_row, text="Limiter:", width=13).pack(side=tk.LEFT)
        ttk.Radiobutton(
                self.manual_lim_row,
                variable=self.manual_limiter,
                value=False,
                text="Off",
                width=6,
                command=partial(self.update_setting, MVX2USetting.MAN_LIMIT),
                ).pack(side=tk.LEFT)
        ttk.Radiobutton(
                self.manual_lim_row,
                variable=self.manual_limiter,
                value=True,
                text="On",
                width=6,
                command=partial(self.update_setting, MVX2USetting.MAN_LIMIT),
                ).pack(side=tk.LEFT)
        self.manual_lim_row.pack(pady=3, fill=tk.X)

        self.manual_hpf_row = ttk.Frame(self.manual_frame)
        ttk.Label(self.manual_hpf_row, text="HPF:", width=13).pack(side=tk.LEFT)
        ttk.Radiobutton(
                self.manual_hpf_row,
                variable=self.manual_hpf,
                value="Off",
                text="Off",
                width=6,
                command=partial(self.update_setting, MVX2USetting.MAN_HPF),
                ).pack(side=tk.LEFT)
        ttk.Radiobutton(
                self.manual_hpf_row,
                variable=self.manual_hpf,
                value="75 Hz",
                text="75 Hz",
                width=6,
                command=partial(self.update_setting, MVX2USetting.MAN_HPF),
                ).pack(side=tk.LEFT)
        ttk.Radiobutton(
                self.manual_hpf_row,
                variable=self.manual_hpf,
                value="150 Hz",
                text="150 Hz",
                width=6,
                command=partial(self.update_setting, MVX2USetting.MAN_HPF),
                ).pack(side=tk.LEFT)
        self.manual_hpf_row.pack(pady=3, fill=tk.X)

        # - - equalizer
        self.manual_eq_row = ttk.Frame(self.manual_frame)
        ttk.Checkbutton(
                self.manual_eq_row,
                variable=self.manual_eq_enabled,
                text="EQ",
                width=5,
                command=partial(self.update_setting, MVX2USetting.MAN_EQ_EN),
                ).pack(side=tk.LEFT)
        # TODO: underneath "EQ Enable" add an "EQ Reset" button

        self.manual_col100 = ttk.Frame(self.manual_eq_row)
        ttk.Label(self.manual_col100, text="100", width=5, anchor=tk.CENTER).pack(side=tk.TOP)
        ttk.Checkbutton(
                self.manual_col100,
                variable=self.manual_eq_100hz_en,
                command=partial(self.update_setting, MVX2USetting.MAN_100HZ_EN),
                ).pack(side=tk.TOP)
        ttk.Scale(
                self.manual_col100,
                variable=self.manual_eq_100hz_db,
                from_=7,
                to=-7,
                length=78,
                orient=tk.VERTICAL,
                command=partial(self.update_setting, MVX2USetting.MAN_100HZ_DB),
                ).pack(side=tk.TOP)
        self.manual_eq_100_val = ttk.Label(self.manual_col100, text="+0")
        self.manual_eq_100_val.pack(side=tk.TOP)
        self.manual_col100.pack(side=tk.LEFT, padx=4, pady=3, fill=tk.BOTH, expand=True)

        self.manual_col250 = ttk.Frame(self.manual_eq_row)
        ttk.Label(self.manual_col250, text="250", width=5, anchor=tk.CENTER).pack(side=tk.TOP)
        ttk.Checkbutton(
                self.manual_col250,
                variable=self.manual_eq_250hz_en,
                command=partial(self.update_setting, MVX2USetting.MAN_250HZ_EN),
                ).pack(side=tk.TOP)
        ttk.Scale(
                self.manual_col250,
                variable=self.manual_eq_250hz_db,
                from_=7,
                to=-7,
                length=78,
                orient=tk.VERTICAL,
                command=partial(self.update_setting, MVX2USetting.MAN_250HZ_DB),
                ).pack(side=tk.TOP)
        self.manual_eq_250_val = ttk.Label(self.manual_col250, text="+0")
        self.manual_eq_250_val.pack(side=tk.TOP)
        self.manual_col250.pack(side=tk.LEFT, padx=4, pady=3, fill=tk.BOTH, expand=True)

        self.manual_col1000 = ttk.Frame(self.manual_eq_row)
        ttk.Label(self.manual_col1000, text="1K", width=5, anchor=tk.CENTER).pack(side=tk.TOP)
        ttk.Checkbutton(
                self.manual_col1000,
                variable=self.manual_eq_1khz_en,
                command=partial(self.update_setting, MVX2USetting.MAN_1KHZ_EN),
                ).pack(side=tk.TOP)
        ttk.Scale(
                self.manual_col1000,
                variable=self.manual_eq_1khz_db,
                from_=7,
                to=-7,
                length=78,
                orient=tk.VERTICAL,
                command=partial(self.update_setting, MVX2USetting.MAN_1KHZ_DB),
                ).pack(side=tk.TOP)
        self.manual_eq_1000_val = ttk.Label(self.manual_col1000, text="+0")
        self.manual_eq_1000_val.pack(side=tk.TOP)
        self.manual_col1000.pack(side=tk.LEFT, padx=4, pady=3, fill=tk.BOTH, expand=True)

        self.manual_col4000 = ttk.Frame(self.manual_eq_row)
        ttk.Label(self.manual_col4000, text="4K", width=5, anchor=tk.CENTER).pack(side=tk.TOP)
        ttk.Checkbutton(
                self.manual_col4000,
                variable=self.manual_eq_4khz_en,
                command=partial(self.update_setting, MVX2USetting.MAN_4KHZ_EN),
                ).pack(side=tk.TOP)
        ttk.Scale(
                self.manual_col4000,
                variable=self.manual_eq_4khz_db,
                from_=7,
                to=-7,
                length=78,
                orient=tk.VERTICAL,
                command=partial(self.update_setting, MVX2USetting.MAN_4KHZ_DB),
                ).pack(side=tk.TOP)
        self.manual_eq_4000_val = ttk.Label(self.manual_col4000, text="+0")
        self.manual_eq_4000_val.pack(side=tk.TOP)
        self.manual_col4000.pack(side=tk.LEFT, padx=4, pady=3, fill=tk.BOTH, expand=True)

        self.manual_col10000 = ttk.Frame(self.manual_eq_row)
        ttk.Label(self.manual_col10000, text="10K", width=5, anchor=tk.CENTER).pack(side=tk.TOP)
        ttk.Checkbutton(
                self.manual_col10000,
                variable=self.manual_eq_10khz_en,
                command=partial(self.update_setting, MVX2USetting.MAN_10KHZ_EN),
                ).pack(side=tk.TOP)
        ttk.Scale(
                self.manual_col10000,
                variable=self.manual_eq_10khz_db,
                from_=7,
                to=-7,
                length=78,
                orient=tk.VERTICAL,
                command=partial(self.update_setting, MVX2USetting.MAN_10KHZ_DB),
                ).pack(side=tk.TOP)
        self.manual_eq_10000_val = ttk.Label(self.manual_col10000, text="+0")  # TODO: assign this value and all others using update_label()
        self.manual_eq_10000_val.pack(side=tk.TOP)
        self.manual_col10000.pack(side=tk.LEFT, padx=4, pady=3, fill=tk.BOTH, expand=True)

        ttk.Label(
                self.manual_eq_row,
                width=5,
                ).pack(side=tk.RIGHT)
        self.manual_eq_row.pack(pady=5, fill=tk.X)

        # - Status
        self.status_bar = ttk.Frame(self)
        ttk.Separator(self.status_bar, orient=tk.HORIZONTAL).pack(pady=0, fill=tk.X, expand=True)
        ttk.Button(
                self.status_bar,
                text="Firmware",
                padding="-2 -2 -2 -2",
                # TODO: implement this dialog
                ).pack(side=tk.RIGHT)
        self.status_label = ttk.Label(self.status_bar)
        self.status_label.pack(side=tk.LEFT)
        self.status_bar.pack(side=tk.BOTTOM, fill=tk.X, expand=True, anchor=tk.S)
        # END Window/Variables/UI Elements

        self.set_active_panel("auto")
        self.get_elements_from_device(startup=True)

    def run(self):
        self.mainloop()

    def handle_button_press(self, event):
        # TODO
        print(event)


if __name__ == "__main__":
    app = ShuxApp()
    app.run()
