from enum import Enum

class Packet(Enum):
    REPORT_ID          = b'\x01'
    SHURE_HEADER_START = b'\x11\x22'
    SHURE_HEADER_END   = b'\x08'
    SHURE_DATA_START   = b'\x70'
    CMD_GET_LOCK       = b'\x01\x02\x01'
    RES_GET_LOCK       = b'\x03\x02\x01'
    CMD_SET_LOCK       = b'\x02\x02\x01'
    RES_SET_LOCK       = b'\x04\x02\x01'
    CMD_GET_FEAT       = b'\x01\x02\x02'
    RES_GET_FEAT       = b'\x03\x02\x02'
    CMD_SET_FEAT       = b'\x02\x02\x02'
    CMD_CONFIRM        = b'\x01\x00\x00'
    RES_CONFIRM        = b'\x09\x00\x00'
    RES_SET_FEAT       = b'\x04\x02\x02'

# TODO: check that list for "valid responses", throw a warning when something new pops up
Commands = [Packet.CMD_GET_LOCK,
            Packet.CMD_SET_LOCK,
            Packet.CMD_GET_FEAT,
            Packet.CMD_SET_FEAT,
            Packet.CMD_CONFIRM]

Responses = [Packet.RES_GET_LOCK,
             Packet.RES_SET_LOCK,
             Packet.RES_GET_FEAT,
             Packet.RES_CONFIRM,
             Packet.RES_SET_FEAT]

