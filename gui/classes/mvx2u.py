import hid
import os

from classes.dummyadapter import *
from classes.features import *
from classes.packets import *
from classes.subscriber import *
from collections import namedtuple
from utils import *

#DEBUG = print  # TODO
DEBUG = lambda *v,**k: None

PEQBand = namedtuple("PEQBand", "id frequencyHz gainDbEnum gainDbx10 enabledEnum enabled")

class ShureMVX2U:
    _configLock = None
    _phantomPowerVolts = None
    _micMute = None
    _monitorMix = None
    _autoLevel = None
    _autoLevelPosition = None
    _autoLevelTone = None
    _autoLevelConfig = None
    _inputGain = None
    _peqEnabled = None
    _peqBands = None
    _highPassFilterMode = None
    _limiterSelect = None
    _compressorMode = None
    _currentRateHz = None
    mv_dev = None
    subscriber = None
    subscriber_should_close = False
    seq = 0
    num_sent = 0
    num_resp = 0
    change_queue = []

    def __init__(self, args=None):
        DEBUG('Init ShureMVX2U Class')

        self.args = args

        if args and args.dummy:
            DEBUG("** skipping USB device detection! **")
            self.mv_dev = DummyMVX2U()
        else:
            DEBUG('HID:', hid.enumerate())
            self.mv_dev = hid.device()
            try:
                self.mv_dev.open(vendor_id=0x14ED, product_id=0x1013)
            except OSError as e:
                raise ConnectionError('Device not found')
            self.mv_dev.set_nonblocking(True)
            assert 'Shure' in self.mv_dev.get_manufacturer_string()
            assert 'MVX2U' in self.mv_dev.get_product_string()
            DEBUG('`mv_dev` now has a handle to a USB-connected MVX2U')

        self.subscriber = MVX2USubscriber(self)
        self.subscriber.start()

        DEBUG('An object of ShureMVX2U class was initialized')
        pass

    def __repr__(self):
        return 'ShureMVX2U()'

    def set_lock_state(self, val_lock_byte):
        # TODO: move into set_feat_state eventually
        DEBUG('Set Lock State')
        pkt1 = self.build_packet(Packet.CMD_SET_LOCK.value, b'\x06' + Feat.LOCK.value + val_lock_byte)
        pkt2 = self.build_packet(Packet.CMD_CONFIRM.value, b'')

        self.send_packet(pkt1)
        self.send_packet(pkt2)

    def set_feat_state(self, feat, val_bytes):
        DEBUG('Set Feat State')
        assert type(feat.value) is bytes
        is_mix = b'\x01' if feat == Feat.MIX else b'\x00'
        # TODO/FIXME: is_mix + feat.value
        pkt1 = self.build_packet(Packet.CMD_SET_FEAT.value, is_mix + feat.value + val_bytes)
        pkt2 = self.build_packet(Packet.CMD_CONFIRM.value, b'')

        self.send_packet(pkt1)
        self.send_packet(pkt2)

    def request_feat_state(self, feat):
        DEBUG('Get Feat State')
        is_mix_or_lock = (b'\x06' if feat == Feat.LOCK else
                          b'\x01' if feat == Feat.MIX else
                          b'\x00')
        pkt = self.build_packet(Packet.CMD_GET_LOCK.value if feat == Feat.LOCK else Packet.CMD_GET_FEAT.value,
                                is_mix_or_lock + feat.value)

        self.send_packet(pkt)

    def build_packet(self, cmd, data):
        assert Packet(cmd) in Commands
        DEBUG('(Build Packet)')

        xmission = (cmd
                    + data)
        xmission = (Packet.SHURE_DATA_START.value
                    + bytes([2 + len(xmission)])
                    + xmission)
        xmission = (Packet.SHURE_HEADER_START.value
                    + bytes([self.seq, 0x03])
                    + Packet.SHURE_HEADER_END.value
                    + xmission[1:2]
                    + xmission)  # TODO: why sometimes 0x0, often 0x3??
        xmission = (Packet.REPORT_ID.value
                    + bytes([2 + len(xmission)])
                    + xmission)
        xmission = CrcEm(xmission)
        xmission = PadEm(xmission)

        self.seq = (self.seq + 1) % 0x100

        return xmission

    def interpret_packet(self, data):
        DEBUG(f'Recv: {HexEm(data)}')

        # Format check
        contents_end = data[1]
        chksum = data[contents_end:contents_end+2]
        # TODO: move into tests folder
        # TODO/FIXME: --dummy --lock
        try:
            assert all(a == b for a, b in zip(data[contents_end:contents_end+2], divmod(calc.checksum(data[2:contents_end]), 0x100)))
        except AssertionError:
            os._exit(7)
        assert Packet(data[2:4]) == Packet.SHURE_HEADER_START
        assert Packet(data[6:7]) == Packet.SHURE_HEADER_END
        assert data[7:8] == data[9:10]  # Data length
        assert Packet(data[8:9]) == Packet.SHURE_DATA_START
        assert (response_type := Packet(data[10:13])) in Responses

        pkt_nr = data[4]
        if response_type == Packet.RES_GET_LOCK:
            resp_t_str = 'GET LOCK'
        elif response_type == Packet.RES_SET_LOCK:
            resp_t_str = 'SET LOCK'
        elif response_type == Packet.RES_GET_FEAT:
            resp_t_str = 'GET FEAT'
        elif response_type == Packet.RES_SET_FEAT:
            resp_t_str = 'SET FEAT'
        elif response_type == Packet.RES_CONFIRM:
            resp_t_str = 'CONFIRM'

        DEBUG(f' Packet #{pkt_nr} is a {resp_t_str} response.', end='')
        if resp_t_str == 'CONFIRM':
            DEBUG('')
            return

        response_key = Feat(data[14:16])
        response_val = int.from_bytes(data[16:16+response_key.bytelen], 'big')
        DEBUG(f' {response_key} reports value {response_val}')

        if response_key.cls_var[0:9] == '_peqBands':
            # Gee, this is fugly
            band_num = int(response_key.cls_var[10])
            assignment = response_key.cls_var[13:]
            band_obj = getattr(self, '_peqBands')[band_num]
            getattr(self, '_peqBands')[band_num] = band_obj._replace(**{assignment: response_val})
        else:
            setattr(self, response_key.cls_var, response_val)

        self.num_resp += 1

    def send_packet(self, packet):
        DEBUG(f'Send: {HexEm(packet)}')
        self.mv_dev.write(packet)
        self.num_sent += 1

    def read_config(self):
        DEBUG('READ CONFIG')

        # Common settings
        self.request_feat_state(Feat.LOCK)
        self.request_feat_state(Feat.VOLTS)
        self.request_feat_state(Feat.MUTE)
        self.request_feat_state(Feat.MIX)

        # Auto
        self.request_feat_state(Feat.AUTO)
        self.request_feat_state(Feat.A_POS)
        self.request_feat_state(Feat.A_TONE)
        self.request_feat_state(Feat.A_GAIN)

        # Manual
        self.request_feat_state(Feat.M_GAIN)
        self.request_feat_state(Feat.EQ)

        self._peqBands = [band for band in map(
            PEQBand._make,
            [("100", 100, Feat.EQ100, -1, Feat.EQ100_EN, -1),
             ("250", 250, Feat.EQ250, -1, Feat.EQ250_EN, -1),
             ("1k", 1000, Feat.EQ1K, -1, Feat.EQ1K_EN, -1),
             ("4k", 4000, Feat.EQ4K, -1, Feat.EQ4K_EN, -1),
             ("10k", 10000, Feat.EQ10K, -1, Feat.EQ10K_EN, -1)])]
        for i, band in enumerate(self._peqBands):
            self.request_feat_state(band.gainDbEnum)  # TODO: name? assigned correctly?
            self.request_feat_state(band.enabledEnum)

        self.request_feat_state(Feat.HPF)
        self.request_feat_state(Feat.LIMIT)
        self.request_feat_state(Feat.COMP)
        self._currentRateHz = 48000  # TODO: does this ever change?

        # TODO: implement a timeout here, and maybe a handler for "too many responses"
        while self.num_resp < self.num_sent:
            time.sleep(0.1)

        DEBUG('Everything requested has an answer')

    ### def find_changes(self):
    ###     DEBUG('*** FINDING CHANGES ***')
    ###     lock_state = self._configLock
    ###     if self.args.unlock is True:
    ###         if lock_state == 1 or self.args.apply_unchanged:
    ###             self.change_queue.append((Feat.LOCK, b'\x00'))
    ###             lock_state = 0
    ###     elif lock_state == 1 and len(sys.argv) > 1:
    ###         print('No changes allowed without --unlock', file=_istdout)
    ###         keys = [key for key in vars(self.args)]
    ###         for key in keys:
    ###             setattr(self.args, key, None)

    ###     if self.args.reset_eq:
    ###         DEBUG('Sending "Reset EQ" settings to queue')  # TODO: is there a dedicated command for this?
    ###         self.change_queue.append((Feat.EQ100_EN, b'\x01'))
    ###         self.change_queue.append((Feat.EQ250_EN, b'\x01'))
    ###         self.change_queue.append((Feat.EQ1K_EN,  b'\x01'))
    ###         self.change_queue.append((Feat.EQ4K_EN,  b'\x01'))
    ###         self.change_queue.append((Feat.EQ10K_EN, b'\x01'))
    ###         self.change_queue.append((Feat.EQ100, b'\x00\x00'))
    ###         self.change_queue.append((Feat.EQ250, b'\x00\x00'))
    ###         self.change_queue.append((Feat.EQ1K,  b'\x00\x00'))
    ###         self.change_queue.append((Feat.EQ4K,  b'\x00\x00'))
    ###         self.change_queue.append((Feat.EQ10K, b'\x00\x00'))

    ###     for fp in Feat:
    ###         DEBUG(f'Getting var names from {str(fp)+":":14} args.{fp.args_var:13} would set ShureMVX2U.{fp.cls_var+":":24}', end='')
    ###         if (assign_val := getattr(self.args, fp.args_var)) is None:
    ###             DEBUG('Flag unused')
    ###             continue
    ###         existing_val = getattr(self, fp.cls_var)
    ###         if assign_val == existing_val:
    ###             DEBUG(f'Value ({existing_val}) is unchanged', end=', setting anyway\n' if self.args.apply_unchanged else '\n')
    ###             if not self.args.apply_unchanged:
    ###                 continue
    ###         else:
    ###             DEBUG(f'{existing_val} --> {assign_val}')  # TODO: ensure existing_val has the same domain as assign_val!

    ###         match fp.args_var:
    ###             case 'manual_gain':
    ###                 assign_val = int(100 * assign_val)

    ###         self.change_queue.append((fp, assign_val.to_bytes(fp.bytelen, 'big')))

    ###     if self.args.band_state is not None or self.args.band_gain is not None:
    ###         # TODO-debug: figure out how the argument list looks for band_state and band_gain
    ###         print(self.args)
    ###         print('fish')
    ###         sys.exit(5)

    ###     if self.args.lock is not None and lock_state == 0:
    ###         self.change_queue.append((Feat.LOCK, b'\x01'))

    def print_config(self):
        DEBUG('*** CURRENT CONFIG ***')

        print("Locked" if self._configLock else "Unlocked")

        print("Muted" if self._micMute else "Not muted")

        # TODO: print("Active preset:", self._activePresetIndex)

        print("Phantom power" if self._phantomPowerVolts > 0 else "No phantom power")

        print("Monitor mix:", self._monitorMix)

        print("Auto level" if self._autoLevel else "Manual mode")

        print("  -- AUTO LEVEL --  ")
        print("Mic Position: ", self._autoLevelPosition)
        print("Tone: ", self._autoLevelTone)
        print("Gain: ", self._autoLevelConfig)

        print(" -- MANUAL LEVEL -- ")
        print("Gain: ", self._inputGain / 100)
        print("EQ active" if self._peqEnabled else "EQ off")
        for band in self._peqBands:
            gain = band.gainDbx10 // 10  # TODO: units in varname correct? (specifically, "Bel" in place of dB)
            is_on = "on" if band.enabled else "off"
            print(f"{band.id:3} Hz: {gain:2} dB ({is_on})")
        print("Compressor:", self._compressorMode)
        print("HPF:", self._highPassFilterMode)
        print("Limiter:", self._limiterSelect)
        print(f"Rate: {self._currentRateHz} Hz")

    def write_config(self):
        DEBUG('WRITE CONFIG')
        DEBUG('Queue:', ', '.join([feat.name + ': ' + HexEm(val) for feat, val in self.change_queue]))
        while self.change_queue:
            feat, val = self.change_queue.pop(0)
            if feat == Feat.LOCK:
                val_is_zero = val == b"\x00"
                DEBUG(f'{"Unl" if val_is_zero else "L"}ocking...')
                self.set_lock_state(val)
            else:
                DEBUG(f'Writing {HexEm(val)} to {feat}')
                self.set_feat_state(feat, val)
