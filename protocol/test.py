from utils import HexEm
from __init__ import *


print('START TEST')
print('-----')

# Check enum types work
print(Dir.RESPONSE)
print(Dir.RESPONSE.value)

a = PacketType.GET_PARAM_REQ
print (a, a.header_byte(), a.data_bytes(), a.dir())

b = Parameter.LIMIT
print (b, b.tuple(), b.bytes(), b.param_len(), b.var_name())

print('-----')

# Get packet type from tuple
p = (4, 2)
print(f'data {p}')
print(f'getting packet type from data: {PacketType.by_value(p)}')

print('-----')

# Get packet type from bytes
pp = bytes((4, 2,))
print(f'data {pp}')
print(f'getting packet type from data: {PacketType.by_value(pp)}')

print('-----')

q = Parameter.by_var_name('mix')
print('expect mix:', q)

print('-----')

# Check ser/de works
var_dict = {}
var_dict['auto_position'] = bytes([0,])

pkts = []

pkts.append(MVX2UPacket(
        sequence_num=0x87,
        packet_type=PacketType.GET_PARAM_REQ,
        param_data=Parameter.A_POS.bytes(),
        ))
pkts.append(MVX2UPacket(
        sequence_num=0x1d,
        packet_type=PacketType.GET_PARAM_RES,
        param_data=Parameter.A_POS.bytes() + bytes([1,]),
        ))
pkts.append(MVX2UPacket(
        sequence_num=0x88,
        packet_type=PacketType.SET_PARAM_REQ,
        param_data=Parameter.A_POS.bytes() + var_dict[Parameter.A_POS.var_name()],
        ))
pkts.append(MVX2UPacket(
        sequence_num=0x89,
        packet_type=PacketType.MAYBE_ACK_REQ,
        param_data=bytes([0,]),
        ))
pkts.append(MVX2UPacket(
        sequence_num=0x1e,
        packet_type=PacketType.MAYBE_ACK_RES,
        param_data=bytes([0, 1,]),
        ))
pkts.append(MVX2UPacket(
        sequence_num=0x1f,
        packet_type=PacketType.SET_PARAM_RES,
        param_data=Parameter.A_POS.bytes() + var_dict[Parameter.A_POS.var_name()],
        ))

for pkt in pkts:
    print(f'      packet: {pkt}')

for pkt in pkts:
    s = serialize(pkt)
    print(f'  serialized: {HexEm(s)}')

for pkt in pkts:
    s = serialize(pkt)
    ds = deserialize(s)
    print(f'deserialized: {ds}')

print('-----')
print('TEST FINISHED!')
