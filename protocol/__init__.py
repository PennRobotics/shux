from enum import Enum, auto
from typing import NamedTuple, Optional

from utils import CrcEm, HexEm, PadEm


class Dir(Enum):
    REQUEST = auto()
    RESPONSE = auto()


class PacketType(Enum):
    # header byte, data bytes, direction
    GET_PARAM_REQ = (3, (1, 2), Dir.REQUEST)
    GET_PARAM_RES = (3, (3, 2), Dir.RESPONSE)
    SET_PARAM_REQ = (0, (2, 2), Dir.REQUEST)
    SET_PARAM_RES = (3, (4, 2), Dir.RESPONSE)
    MAYBE_ACK_REQ = (3, (1, 0), Dir.REQUEST)  # TODO: what is the purpose of this?
    MAYBE_ACK_RES = (3, (9, 0), Dir.RESPONSE)  # TODO: what is the purpose of this?
    # TODO: there should also be error packets when the wrong data are sent!
    # TODO: should the other (non-param, such as serial) values be here?

    def header_byte(self) -> int:
        return self.value[0]

    def data_bytes(self) -> tuple:
        return self.value[1]

    def dir(self) -> Dir:
        return self.value[2]

    @staticmethod
    def by_value(value):
        assert hasattr(value, '__iter__')
        return next(filter(lambda packet_type: packet_type.data_bytes() == tuple(value), PacketType), None)


class Parameter(Enum):
    # bytes, param_len, var_name
    LOCK     = ((1, 0, 0, 0xA6), 1, 'lock')  # TODO-hi: is SET_PARAM for LOCK 0308 or 0008 for 6th/7th packet bytes?
    M_GAIN   = ((2, 0, 1, 0x02), 2, 'manual_gain')  # Each 0.5 dB = 32h (16-bit)
    MUTE     = ((2, 0, 1, 0x04), 1, 'mute')
    HPF      = ((2, 0, 1, 0x06), 1, 'hpf')
    LIMIT    = ((2, 0, 1, 0x51), 1, 'limiter')
    COMP     = ((2, 0, 1, 0x5C), 1, 'compressor')  # double check this!! (should be 0 to 3, right? only 0 to 2 in data dump; TODO)
    VOLTS    = ((2, 0, 1, 0x66), 1, 'phantom')  # 0 or 48 (0x30)
    A_POS    = ((2, 0, 1, 0x82), 1, 'auto_position')  # 1=far
    A_TONE   = ((2, 0, 1, 0x83), 1, 'auto_tone')  # 0=dark, 1=nat, 2=bright
    AUTO     = ((2, 0, 1, 0x85), 1, 'auto_en')  # 1=auto  - TODO: has been renamed!
    MIX      = ((2, 0, 1, 0x86), 1, 'mix')  # 64h (100) is full mic
    A_GAIN   = ((2, 0, 1, 0x87), 4, 'auto_gain')  # 0, 1, 2  - TODO: sure about this name?
    EQ       = ((2, 0, 2, 0x00), 1, 'eq')
    EQ100_EN = ((2, 0, 2, 0x10), 1, 'band1_state')
    EQ100    = ((2, 0, 2, 0x14), 2, 'band1_gain')
    EQ250_EN = ((2, 0, 2, 0x20), 1, 'band2_state')
    EQ250    = ((2, 0, 2, 0x24), 2, 'band2_gain')
    EQ1K_EN  = ((2, 0, 2, 0x30), 1, 'band3_state')
    EQ1K     = ((2, 0, 2, 0x34), 2, 'band3_gain')
    EQ4K_EN  = ((2, 0, 2, 0x40), 1, 'band4_state')
    EQ4K     = ((2, 0, 2, 0x44), 2, 'band4_gain')
    EQ10K_EN = ((2, 0, 2, 0x50), 1, 'band5_state')
    EQ10K    = ((2, 0, 2, 0x54), 2, 'band5_gain')

    def tuple(self):
        return self.value[0]

    def bytes(self):
        return bytes(self.value[0])

    def param_len(self):
        return self.value[1]

    def var_name(self):
        return self.value[2]

    @staticmethod
    def by_value(value: tuple):
        assert isinstance(value, tuple)
        assert len(value) == 4, 'Parameter search argument has wrong length'

        return next(filter(lambda param: param.tuple() == value, Parameter), None)

    @staticmethod
    def by_bytes(b: bytes):
        return self.by_value(tuple(bytes))

    @staticmethod
    def by_var_name(var_name: str):
        return next(filter(lambda param: param.var_name() == var_name, Parameter), None)


class MVX2UPacket(NamedTuple):
    sequence_num: int
    packet_type: PacketType
    param_data: bytes

    def __repr__(self):
        return f'MVX2UPacket(sequence_num={self.sequence_num:#02x}, ' + \
                f'packet_type={self.packet_type}, ' + \
                f'param_data={HexEm(self.param_data)})'


HEADER_LEN = 10
PACKET_TYPE_LEN = 2
CRC_LEN = 2

def serialize(packet: MVX2UPacket) -> bytes:
    return PadEm(CrcEm(bytes([
            0x01,  # first header byte
            HEADER_LEN + PACKET_TYPE_LEN + len(packet.param_data),
            0x11,
            0x22,
            packet.sequence_num,
            packet.packet_type.header_byte(),
            0x08,
            PACKET_TYPE_LEN + len(packet.param_data) + CRC_LEN,
            0x70,
            PACKET_TYPE_LEN + len(packet.param_data) + CRC_LEN,  # last header byte
            *packet.packet_type.data_bytes(),
            *packet.param_data,
            ])))


def deserialize(packet: bytes) -> Optional[MVX2UPacket]:
    assert packet[0] == 0x01 and \
            packet[2] == 0x11 and \
            packet[3] == 0x22 and \
            (packet[5] == 0x00 or packet[5] == 0x03) and \
            packet[6] == 0x08 and \
            packet[8] == 0x70, \
            f'Packet seems corrupt:\n{packet}'

    __crc_offset = packet[1]
    assert __crc_offset > HEADER_LEN, \
            'CRC must be outside header'
    __packet_crc = packet[__crc_offset : __crc_offset + CRC_LEN]
    assert CrcEm(packet[0 : __crc_offset])[-2:] == __packet_crc, \
            'Packet checksum mismatch:\n{packet}'

    __seq_nr = packet[4]

    __payload = packet[HEADER_LEN : __crc_offset]
    assert len(__payload) + CRC_LEN == packet[7] == packet[9], \
            f'Data length is different than reported:\n{packet}'

    __type_bytes, __param_bytes = __payload[:PACKET_TYPE_LEN], __payload[PACKET_TYPE_LEN:]
    __type = PacketType.by_value(__type_bytes)

    return MVX2UPacket(
            sequence_num=__seq_nr,
            packet_type=__type,
            param_data=__param_bytes,
            )
