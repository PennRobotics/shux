* [source a](https://github.com/justjanne/zoom-f2-api/blob/main/protocol/__init__.py)
* [source b](https://github.com/justjanne/bmd-hid-device/blob/main/bmd_hid_device/rawdevice.py)

* The advantage of the simpler source a is that it implements _async_
* source b has a more built-up USB handler

```sh
python3 -m venv .venv
source .venv/bin/activate
pip install crc
python3 __init__.py
deactivate
```
