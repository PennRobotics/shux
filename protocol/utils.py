from crc import Calculator, Configuration

CRC_16_ANSI = Configuration(
    width=16,
    polynomial=0x8005,
    init_value=0x0000,
    final_xor_value=0x0000,
    reverse_input=True,
    reverse_output=True)

calc = Calculator(CRC_16_ANSI)

def CrcEm(ba: bytes):
    return ba + bytes(divmod(calc.checksum(ba[2:]), 0x100))

def PadEm(ba: bytes):
    return ba + bytes([0]) * (64 - len(ba))

def HexEm(ba: bytes):
    return ''.join([f'{b:02x}' for b in ba])

def ByteMe(hexArr):
    return bytes.fromhex(hexArr.ljust(126, '0')) if type(hexArr) is str else bytes(hexArr)
